library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.Constants.ALL;

entity Memory_Cleaner is
port (
	
	i_MC_Clk_125 		: in  std_logic;
	i_MC_FSM_State 		: in FSM_State_Vga;

	o_MC_VRam_Enable	: out std_logic;
	o_MC_VRam_Data 		: out std_logic_vector(2 downto 0);
	o_MC_VRam_Address	: out std_logic_vector(15 downto 0)

	
	);
end Memory_Cleaner;

architecture Behavioral of Memory_Cleaner is
	
	signal r_Counter : integer range 0 to c_VRam_Limit := c_VRam_Limit;
	signal r_Wipe, r_Cleaning : std_logic;
	
	signal r_VRam_Enable		: std_logic;
	signal r_VRam_Data 		: std_logic_vector(2 downto 0);
	signal r_VRam_Address	: std_logic_vector(15 downto 0);	

begin
	
	o_MC_VRam_Enable 	<= r_VRam_Enable;
	o_MC_VRam_Data 		<= r_VRam_Data;
	o_MC_VRam_Address 	<= r_VRam_Address;

	Check_P : process (i_MC_Clk_125)--, i_MC_FSM_State)
	begin
		if i_MC_Clk_125 = '1' and i_MC_Clk_125'Event then
			
			case i_MC_FSM_State is
				when Reset_Vga =>
					r_Wipe <= '0';
					r_Counter <= 0;
				when New_Game_Vga =>

					if (r_Wipe = '0' and r_Counter = 0) then
						r_Wipe <= '1';
						r_VRam_Enable 	<= '1';
					else
						if r_Counter <= c_VRam_Limit then
							
							r_Counter <= r_Counter +1;
						else
							
							
						end if;
					end if;

				when Idle_Vga =>
					r_Wipe <= '0';
					r_Counter <= 0;
					r_VRam_Enable 	<= '0';
			end case;

		end if;
	end process;

	Clean_P : process (r_Counter, r_Wipe, r_VRam_Enable)
	begin

			if ( r_Wipe = '1' and r_Counter < c_VRam_Limit-1) then
				--r_Cleaning <= '1';
				
				r_VRam_Data 	<= "010";--"001";--
				r_VRam_Address	<= std_logic_vector(to_unsigned(r_Counter-1, 16));
				
				--if r_Counter = c_VRam_Limit -11  then
					--r_VRam_Address <= (others => '0');
				--end if;
			elsif r_Wipe = '0' then
				r_VRam_Address <= (others => '0');

			end if;
	end process;

end Behavioral;
