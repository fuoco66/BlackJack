library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Music_Constants.ALL;
use work.Constants.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Music_Module is
port(
	i_Music_Clk			: in std_logic;
	i_Music_On  		: in std_logic;
	--i_Music_Switch  : in std_logic;
	i_Music_FSM_State 	: in FSM_State_Main;

	i_Music_Dealer_Sum	: in integer range 0 to 63;	
	i_Music_Player1_Sum	: in integer range 0 to 63;
	i_Music_Player2_Sum	: in integer range 0 to 63;

	o_Music_Out 		: out std_logic
	);
end Music_Module;

architecture Behavioral of Music_Module is
	
	
	signal r_Counter : integer;
	signal r_Counter_Limit : integer;
	signal r_seconds : integer range 0 to 250000000:=0;

	signal r_Nota, r_Nota_OLD : integer :=0;-- range 0 to c_Music_Pirates'HIGH:=0;
	signal r_Pin, r_Music_Switch_Old :std_logic;
	signal r_MainFSM_OLD : FSM_State_Main;

	TYPE Music_State IS (StopM, Play_Pirates, Play_Win, Play_Loose, Changing );  -- Rename New game in soft-reset

	signal r_Control : Music_State;

begin
	
	mainControl : process (i_Music_Clk)
	begin
		if i_Music_Clk'event and i_Music_Clk = '1' then
			
			if (i_Music_On = '1') then

				case i_Music_FSM_State is
					when Reset =>
						r_Control <= StopM;					
					when Reset_Delay =>
						r_Control <= StopM;					
					when Main_Init =>
						r_Control <= Play_Pirates;					
					when New_Game =>
						r_Control <= Play_Pirates;					
					when Game_Start => --primo turno, quello delle 2 carte coperte
						r_Control <= Play_Pirates;					
					when Game_Play => --secondo turno
						r_Control <= Play_Pirates;					
					when Game_Finish => --qui si scoprono le carte!!!!
						r_Control <= Play_Pirates;					
					when End_Game => --stato finale, 2s, poi va in idle
						if (r_MainFSM_OLD /= i_Music_FSM_State) and (i_Music_FSM_State = End_Game)  then
							r_Control <= Changing;		
						else
							if (i_Music_Player1_Sum > i_Music_Player2_Sum and i_Music_Player1_Sum > i_Music_Dealer_Sum) then --vince P1
								r_Control <= Play_Win;	

							elsif (i_Music_Player2_Sum > i_Music_Player1_Sum and i_Music_Player2_Sum > i_Music_Dealer_Sum) then --vince P2
								r_Control <= Play_Win;	
							
							elsif (i_Music_Player1_Sum = i_Music_Player2_Sum and i_Music_Player1_Sum > i_Music_Dealer_Sum) then --pareggio P1 e P2
								r_Control <= Play_Win;	
							
							elsif (i_Music_Dealer_Sum >= i_Music_Player1_Sum and i_Music_Dealer_Sum >= i_Music_Player2_Sum) then --vince De
								r_Control <= Play_Loose;								
							end if ;
						end if;

						--If sumP1 = sumP2 and sumP1 > sumDe then ==> pareggio P1 e P2

						--if sumP1 > sumP2 and sumP1 > sumDe then ==> vince P1

						--if sumP2 > sumP1 ans sumP2 > sumDe then ==> vince P2

						--if sumDe >= sumP2 and sumDe >= sumP1 then ==> vince De


						



					when Idle =>	--punto di partenza!!!!!!
						r_Control <= Play_Pirates;
				end case;

			else
				r_Control <= StopM;
				
			end if;
				
		end if;
	end process;

	process(i_Music_Clk)
		begin
			if i_Music_Clk'event and i_Music_Clk = '1' then
				
				case r_Control is
					when StopM =>
						r_Nota <= 0;
						r_seconds <= 0;
					when Play_Pirates =>

						if (r_seconds >= to_integer(unsigned( c_Music_Pirates(r_Nota)(35 downto 8) )) and r_Nota < c_Music_Pirates'HIGH) then
							r_seconds <= 0;
							r_Nota <= r_Nota +1;
						elsif r_Nota = c_Music_Pirates'HIGH then
							r_Nota <= 0;
						else
							r_seconds <= r_seconds +1;
							r_Nota <= r_Nota;					
						end if;

					when Play_Win => --stato finale, 2s, poi va in idle
						
						if (r_seconds >= to_integer(unsigned( c_Music_Mario_Win(r_Nota)(35 downto 8) )) and r_Nota < c_Music_Mario_Win'HIGH) then
							r_seconds <= 0;
							r_Nota <= r_Nota +1;
						elsif r_Nota = c_Music_Mario_Win'HIGH then
							--r_Nota <= 0;
						else
							r_seconds <= r_seconds +1;
							r_Nota <= r_Nota;					
						end if;
					
					when Play_Loose => --stato finale, 2s, poi va in idle
						
						if (r_seconds >= to_integer(unsigned( c_Music_Mario_Loose(r_Nota)(35 downto 8) )) and r_Nota < c_Music_Mario_Loose'HIGH) then
							r_seconds <= 0;
							r_Nota <= r_Nota +1;
						elsif r_Nota = c_Music_Mario_Loose'HIGH then
							--r_Nota <= 0;
						else
							r_seconds <= r_seconds +1;
							r_Nota <= r_Nota;					
						end if;

					when Changing =>
						r_Nota <= 0;
						r_seconds <= 0;
					when others =>

				end case;

			end if;
	end process;

	process (i_Music_Clk)
	begin
		if i_Music_Clk'event and i_Music_Clk = '1' then
			
			case r_Control is
				when StopM =>
					r_Counter_Limit <= 0;			
				
				when Play_Pirates =>
					r_Nota_OLD <= r_Nota;
					r_MainFSM_OLD <= i_Music_FSM_State;
					r_Counter_Limit <= c_Music_Notes( to_integer(unsigned(c_Music_Pirates(r_Nota)(7 downto 0))) ) ;
				
				when Play_Win =>
					r_Nota_OLD <= r_Nota;
					r_MainFSM_OLD <= i_Music_FSM_State;
					r_Counter_Limit <= c_Music_Notes( to_integer(unsigned(c_Music_Mario_Win(r_Nota)(7 downto 0))) ) ;

				when Play_Loose =>
					r_Nota_OLD <= r_Nota;
					r_MainFSM_OLD <= i_Music_FSM_State;
					r_Counter_Limit <= c_Music_Notes( to_integer(unsigned(c_Music_Mario_Loose(r_Nota)(7 downto 0))) ) ;
				when Changing =>
					r_Nota_OLD <= r_Nota;
					r_MainFSM_OLD <= i_Music_FSM_State;
					r_Counter_Limit <= c_Music_Notes( to_integer(unsigned(c_Music_Pirates(r_Nota)(7 downto 0))) ) ;
				when others =>
				
			end case;

		end if;
	end process;


	process (i_Music_Clk)
	begin
		if i_Music_Clk'event and i_Music_Clk = '1' then
			if ( r_Control /= StopM ) or ( r_Control /= Changing ) then ------------
				
				if( r_Nota /= r_Nota_OLD and r_Counter_Limit /= 0)then
					r_Counter <= 0;
					r_Pin <= '0';
				elsif ( r_Nota /= r_Nota_OLD and r_Counter_Limit = 0)then
					r_Counter <= 0;
					r_Pin <= '0';
				elsif r_Counter < r_Counter_Limit/2 and r_Counter_Limit /= 0 then
					r_Counter <= r_Counter + 1;				
				elsif r_Counter = r_Counter_Limit/2 and r_Counter_Limit /= 0 then
					r_Pin <= not r_Pin;
					r_Counter <= 0;
				end if;

			else
				r_Counter <= 0;
				r_Pin <= '0';
			end if;

		end if;
	end process;


	o_Music_Out <= r_Pin;


end Behavioral;