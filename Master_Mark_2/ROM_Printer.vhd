library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.Cards_Constants.All;

entity Rom_Printer is

port (
	i_Printer_Clk			: in std_logic;
	i_Printer_Reset 		: in std_logic;

	i_Printer_Request		: in std_logic;
	i_Printer_Code_Object	: in integer range 0 to 63;
	i_Printer_X_Object		: in integer range 0 to 511;
	i_Printer_Y_Object		: in integer range 0 to 127;

	o_Printer_VRam_Select	: out std_logic_vector (0 downto 0);
	o_Printer_VRam_Address	: out std_logic_vector (15 downto 0);
	o_Printer_VRam_Data 	: out std_logic_vector (2 downto 0)
	);
end Rom_Printer;

architecture Behavioral of Rom_Printer is

Component LettersRom2 is		--ogni lettera è di 20x20 pixel ovvero 400 bit
	  port (
	    clka : in std_logic;
	    addra : in std_logic_vector(14 downto 0);
	    douta : out std_logic_vector(2 downto 0)
	  );
	end Component LettersRom2;

	Component MarioRom2 is	--ogni frame è di 44x32 pixel ovvero 1408 bit
	  port (
	    clka : in std_logic;
	    addra : in std_logic_vector(14 downto 0);
	    douta : out std_logic_vector(2 downto 0)
	  );
	end Component MarioRom2;

signal r_Pos_Object : integer range 0 to 18000;
signal r_Code_Object : integer range 0 to 63;
signal r_Offset_X, r_Offset_Y, r_Dimensione_X, r_Dimensione_Y, r_HPOS, r_VPOS : integer range 0 to 512;
signal r_Printer_Printing, r_Cleaning_Down, r_Cleaning_Up : std_logic;
signal r_Counter, r_Counter_Limit : integer range 0 to 1408;
signal r_Address, r_LetterAddress, r_MarioAddress : std_logic_vector (14 downto 0);
signal r_Data,  r_Mario_Data,  r_Letter_Data	: std_logic_vector (2 downto 0);
signal r_Select 	: std_logic_vector (0 downto 0);
signal r_Vram_Data : std_logic_vector (2 downto 0);
signal r_VRam_Address : std_logic_vector (15 downto 0);

begin
	Lett_Rom	: LettersRom2 port map(i_Printer_Clk, r_LetterAddress, r_Letter_Data);
	Mario_Rom	: MarioRom2   port map(i_Printer_Clk, r_MarioAddress, r_Mario_Data);

	process (i_Printer_Clk, i_Printer_Reset)
	begin
		if i_Printer_Reset = '1' then
			r_Offset_X <= 0;
			r_Offset_Y <= 0;
			r_Dimensione_X <= 0;
			r_Dimensione_Y <= 0;
			r_Printer_Printing <= '0';
			r_Cleaning_Down <= '0';
			r_Cleaning_Up <= '0';
			r_Counter <= 0;
			r_LetterAddress <= (others => '0');
			r_MarioAddress <= (others => '0');
			r_Address <= (others => '0');
			r_Data <= (others => '0');
			r_HPOS <= 0;
			r_VPOS <= 0;
			r_Code_Object <= 63;
		elsif i_Printer_Reset = '0' and rising_edge(i_Printer_Clk) then
			
			if i_Printer_Request = '1' and r_Printer_Printing = '0' and r_Cleaning_Down = '0' and r_Cleaning_Up = '0' then
				if i_Printer_Code_Object < 45 then
					r_Printer_Printing <= '1';
					r_Counter_Limit <= 400;
					r_Dimensione_X <= 20;
					r_Dimensione_Y <= 20;
					r_Pos_Object <= i_Printer_Code_Object * 400;
					r_LetterAddress <= std_logic_vector(to_unsigned( (i_Printer_Code_Object * 400) , 15));
				elsif i_Printer_Code_Object >= 46 and i_Printer_Code_Object < 58 then
					r_Printer_Printing <= '1';
					r_Counter_Limit <= 1408;
					r_Dimensione_X <= 44;
					r_Dimensione_Y <= 32;
					r_Pos_Object <= (i_Printer_Code_Object - 46) * 1408;
					r_MarioAddress <= std_logic_vector(to_unsigned( (((i_Printer_Code_Object - 46) * 1408)) , 15));
				elsif i_Printer_Code_Object = 62 then
					r_Dimensione_X <= 512 - i_Printer_X_Object;
					r_Dimensione_Y <= 128 - i_Printer_Y_Object;
					r_Cleaning_Down <= '1';
				elsif i_Printer_Code_Object = 61 then
					r_Dimensione_X <= i_Printer_X_Object;
					r_Dimensione_Y <= i_Printer_Y_Object;
					r_Cleaning_Up <= '1';
				end if;
				r_Offset_X <= i_Printer_X_Object;
				r_Offset_Y <= i_Printer_Y_Object;
				r_Code_Object <= i_Printer_Code_Object;
				r_VRam_Address <= std_logic_vector(to_unsigned((r_Offset_Y), 7)) & std_logic_vector(to_unsigned((r_Offset_X), 9));
				r_Counter <= 1;
			end if;
			if r_Cleaning_Up = '1' then
				r_Select <= "1";
				if r_Select = "1" then
					if r_HPOS < r_Dimensione_X - 1 then
						r_HPOS <= r_HPOS + 1;
						else
						r_HPOS <= 0;
						if r_VPOS < r_Dimensione_Y - 1 then
							r_VPOS <= r_VPOS + 1;
						else
							r_VPOS <= 0;
							r_Cleaning_Up <= '0';
							r_Select <= "0";
						end if ;
					end if ;
				end if;
				r_Vram_Address <= std_logic_vector(to_unsigned((r_VPOS), 7)) & std_logic_vector(to_unsigned((r_HPOS), 9));
				r_Vram_Data <= "010";
			end if;
			if r_Cleaning_Down = '1' then
				r_Select <= "1";
				if r_Select = "1" then
					if r_HPOS < r_Dimensione_X - 1 then
						r_HPOS <= r_HPOS + 1;
						else
						r_HPOS <= 0;
						if r_VPOS < r_Dimensione_Y - 1 then
							r_VPOS <= r_VPOS + 1;
						else
							r_VPOS <= 0;
							r_Cleaning_Down <= '0';
							r_Select <= "0";
						end if ;
					end if ;
				end if;
				r_Vram_Address <= std_logic_vector(to_unsigned((r_Offset_Y+r_VPOS), 7)) & std_logic_vector(to_unsigned((r_Offset_X+r_HPOS), 9));
				r_Vram_Data <= "010";
			end if;
			if r_Printer_Printing = '1' then
				r_Select <= "1";
				if r_Select = "1" then
					if r_HPOS < r_Dimensione_X - 1 then
						r_HPOS <= r_HPOS + 1;
						else
						r_HPOS <= 0;
						if r_VPOS < r_Dimensione_Y - 1 then
							r_VPOS <= r_VPOS + 1;
						else
							r_VPOS <= 0;
							r_Printer_Printing <= '0';
							r_Select <= "0";	--PROBLEMA COI SELECT, NE SERVE UN ALTRO!!!!!
							r_LetterAddress <= "000000000000000";
							r_MarioAddress <= "000000000000000";
						end if ;
					end if ;
				end if;
				if r_Counter < r_Counter_Limit then
					r_Counter <= r_Counter + 1;
				else 
					r_Counter <= 0;
				end if;
				if r_Code_Object < 45 then
					r_LetterAddress <= std_logic_vector(to_unsigned( (r_Counter + r_Pos_Object) , 15));
				elsif r_Code_Object > 45 and r_Code_Object < 58 then 
					r_MarioAddress <= std_logic_vector(to_unsigned( (r_Counter + r_Pos_Object) , 15));
				end if;
				r_Vram_Address <= std_logic_vector(to_unsigned((r_Offset_Y+r_VPOS), 7)) & std_logic_vector(to_unsigned((r_Offset_X+r_HPOS), 9));
				if r_Code_Object < 46 then
					r_Vram_Data <= r_Letter_Data;
				elsif r_Code_Object >= 46 and r_Code_Object < 58 then
					r_Vram_Data <= r_Mario_Data;
				end if;
			end if;
		end if;
	end process;
	o_Printer_VRam_Select	<= r_Select;
	o_Printer_VRam_Address	<= r_Vram_Address;
	o_Printer_VRam_Data 	<= r_Vram_Data;

end Behavioral;