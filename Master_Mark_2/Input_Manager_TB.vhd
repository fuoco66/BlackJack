--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:26:52 06/11/2016
-- Design Name:   
-- Module Name:   D:/Develop/Xlinx/PROJECT/Master_Mark_2/Input_Manager_TB.vhd
-- Project Name:  Master_Mark_2
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Input_Manager
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Input_Manager_TB IS
END Input_Manager_TB;
 
ARCHITECTURE behavior OF Input_Manager_TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Input_Manager
    PORT(
         i_IM_Clk_125 : IN  std_logic;
         i_IM_Buttons : IN  std_logic_vector(2 downto 0);
         o_IM_Buttons : OUT  std_logic_vector(2 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal i_IM_Clk_125 : std_logic := '0';
   signal i_IM_Buttons : std_logic_vector(2 downto 0) := (others => '0');

 	--Outputs
   signal o_IM_Buttons : std_logic_vector(2 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   constant i_IM_Clk_125_period : time := 8 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Input_Manager PORT MAP (
          i_IM_Clk_125 => i_IM_Clk_125,
          i_IM_Buttons => i_IM_Buttons,
          o_IM_Buttons => o_IM_Buttons
        );

   -- Clock process definitions
   i_IM_Clk_125_process :process
   begin
		i_IM_Clk_125 <= '0';
		wait for i_IM_Clk_125_period/2;
		i_IM_Clk_125 <= '1';
		wait for i_IM_Clk_125_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      i_IM_Buttons(0) <= '1';
      wait for i_IM_Clk_125_period*20;
      i_IM_Buttons(0) <= '0';		
      wait for i_IM_Clk_125_period*20;
      i_IM_Buttons(1) <= '1';
      wait for i_IM_Clk_125_period*20;
      i_IM_Buttons(1) <= '0';		
      wait for i_IM_Clk_125_period*20;
		i_IM_Buttons(2) <= '1';
      wait for i_IM_Clk_125_period*20;
      i_IM_Buttons(2) <= '0';		
      wait for i_IM_Clk_125_period*20;
		i_IM_Buttons <= "111";
      wait for i_IM_Clk_125_period*20;
      i_IM_Buttons <= "000";		
      

      -- insert stimulus here 

      wait;
   end process;

END;
