library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.Constants.ALL;
use work.Printer_Constants.ALL;



entity PrinterManager is
port(
	i_PrintModule_Clk : in std_logic;
	i_PrintModule_Reset	: in std_logic;

	i_PrintModule_State : in FSM_state_Main;
	i_PrintModule_Turn	: in std_logic_vector (1 downto 0);
	
	i_Ps_Dealer_Sum		: in integer range 0 to 63;
	i_Ps_Player1_Sum	: in integer range 0 to 63;
	i_Ps_Player2_Sum	: in integer range 0 to 63;

	o_Printmodule_Request : out std_logic;
	o_Printmodule_Code_Object	: out integer range 0 to 63;
	o_Printmodule_X_Object		: out integer range 0 to 511;
	o_Printmodule_Y_Object		: out integer range 0 to 127
	);
end PrinterManager;

architecture Behavioral of PrinterManager is

	signal r_Printmodule_Request : std_logic;
	signal r_Printmodule_Code_Object :integer range 0 to 63;
	signal r_Printmodule_X_Object	: integer range 0 to 511;
	signal r_Printmodule_Y_Object	: integer range 0 to 127;

	signal r_DelayOk, r_DelayStart, r_Flag_0, r_Flag_1, r_Flag_2, r_Flag_3, r_Flag_4, r_Flag_5, r_Flag_6, r_Flag_7, r_Flag_8, r_Flag_9, r_Cleaned 	: std_logic;
	signal r_Delay_Counter, r_Command_Counter, r_Delay_Move_Counter, r_Delay_Power	: integer;


begin

	r_DelayOk <= '1' when (r_Delay_Counter = 2**r_Delay_Power) else '0'; --2000

	Delay_proc :process (i_PrintModule_Clk)
	begin
		if i_PrintModule_Reset = '1' then
			r_Delay_Counter <= 0;
		end if;
		if (i_PrintModule_Clk = '1' and i_PrintModule_Clk'event) then
			if r_DelayOk = '1' then
				r_Delay_Counter <= 0;
			elsif r_DelayStart = '1' then
				r_Delay_Counter <= r_Delay_Counter + 1;
			end if;
		end if;
	end process;

	process (i_PrintModule_Clk)
	begin
		if (i_PrintModule_Clk = '1' and i_PrintModule_Clk'event) then
			if (i_PrintModule_Reset = '1') then
				r_Printmodule_Request <= '0';
				r_Printmodule_Y_Object <= 0;
				r_Printmodule_X_Object <= 0;
				r_Printmodule_Code_Object <= 0;
				r_DelayStart <= '0';
				r_Flag_0 <= '0';
				r_Flag_1 <= '0';
				r_Flag_2 <= '0';
				r_Flag_3 <= '0';
				r_Flag_4 <= '0';
				r_Flag_5 <= '0';
				r_Flag_6 <= '0';
				r_Flag_7 <= '0';
				r_Flag_8 <= '0';
				r_Flag_9 <= '0';
				r_Cleaned <= '0';
				r_Delay_Power <= c_Clean_Delay;
			else
				if (i_PrintModule_State = New_Game) then
					if r_DelayOk = '0' and r_DelayStart = '0' and r_Cleaned = '0' then
						r_Printmodule_Request <= '1';
						r_Printmodule_Code_Object <= 62;
						r_Printmodule_X_Object <= 0;
						r_Printmodule_Y_Object <= 0;
						r_DelayStart <= '1';
						r_Delay_Power <= c_Clean_Delay;
						r_Command_Counter <= 0;
					
					elsif r_DelayStart = '1' and r_DelayOk = '0' and r_Cleaned = '0' then
						r_Printmodule_Request <= '0';
					
					elsif r_DelayOk = '1' then
							r_Cleaned <= '1';
					end if;
				elsif (i_PrintModule_State = Main_Init) then
					if r_DelayOk = '0' and r_DelayStart = '0' and r_Flag_0 = '0' then
						r_Printmodule_Request <= '1';
						r_Printmodule_Code_Object <= 62;
						r_Printmodule_X_Object <= 0;
						r_Printmodule_Y_Object <= 0;
						r_DelayStart <= '1';
						r_Delay_Power <= c_Clean_Delay + 3;
						r_Command_Counter <= 0;
					
					elsif r_DelayStart = '1' and r_DelayOk = '0' and r_Flag_0 = '0' then
						r_Printmodule_Request <= '0';
					
					elsif r_DelayOk = '1' and r_Flag_0 = '0' then
							r_Command_Counter <= r_Command_Counter + 1;
							
							if r_Command_Counter < (c_Printer_Command_BJ'HIGH + 1) then
								r_Printmodule_Request <= '1';
								r_Printmodule_Code_Object <= to_integer(unsigned(c_Printer_Command_BJ(r_Command_Counter)(21 downto 16)));
								r_Printmodule_X_Object <= to_integer(unsigned(c_Printer_Command_BJ(r_Command_Counter)(15 downto 7)));
								r_Printmodule_Y_Object <= to_integer(unsigned(c_Printer_Command_BJ(r_Command_Counter)(6 downto 0)));
								r_Delay_Power <= to_integer(unsigned(c_Printer_Command_BJ(r_Command_Counter)(26 downto 22)));
							else
								r_Printmodule_Request <= '0';
								r_Command_Counter <= 0;
								r_DelayStart <= '0';
								r_Flag_0 <= '1';
							end if;

					end if;
				elsif (i_PrintModule_State = Idle) then
					if r_DelayOk = '0' and r_DelayStart = '0' and r_Flag_1 = '0' then
						r_Printmodule_Request <= '1';
						r_Printmodule_Code_Object <= 62;
						r_Printmodule_X_Object <= 0;
						r_Printmodule_Y_Object <= 50;
						r_DelayStart <= '1';
						r_Delay_Power <= c_Clean_Delay;
						r_Command_Counter <= 0;
					
					elsif r_DelayStart = '1' and r_DelayOk = '0' and r_Flag_1 = '0' then
						r_Printmodule_Request <= '0';
					
					elsif r_DelayOk = '1' and r_Flag_1 = '0' then
							r_Command_Counter <= r_Command_Counter + 1;
							
							if r_Command_Counter < (c_Printer_Command_New'HIGH + 1) then
								r_Printmodule_Request <= '1';
								r_Printmodule_Code_Object <= to_integer(unsigned(c_Printer_Command_New(r_Command_Counter)(21 downto 16)));
								r_Printmodule_X_Object <= to_integer(unsigned(c_Printer_Command_New(r_Command_Counter)(15 downto 7)));
								r_Printmodule_Y_Object <= to_integer(unsigned(c_Printer_Command_New(r_Command_Counter)(6 downto 0)));
								r_Delay_Power <= to_integer(unsigned(c_Printer_Command_New(r_Command_Counter)(26 downto 22)));
							else
								r_Printmodule_Request <= '0';
								r_Command_Counter <= 0;
								r_DelayStart <= '0';
								r_Flag_1 <= '0';
							end if;

					end if;					
				elsif (i_PrintModule_State = Game_Start and i_PrintModule_Turn = "01") then
					if r_DelayOk = '0' and r_DelayStart = '0' and r_Flag_2 = '0' then
						r_Printmodule_Request <= '1';
						r_Printmodule_Code_Object <= 62;
						r_Printmodule_X_Object <= 0;
						r_Printmodule_Y_Object <= 0;
						r_Delay_Power <= c_Clean_Delay;
						r_DelayStart <= '1';
						r_Command_Counter <= 0;
					elsif r_DelayStart = '1' and r_DelayOk = '0' and r_Flag_2 = '0' then
						r_Printmodule_Request <= '0';
					elsif r_DelayOk = '1' and r_Flag_2 = '0' then
						r_Command_Counter <= r_Command_Counter + 1;
						
						if r_Command_Counter < (c_Printer_Command_Draw'HIGH + 1) then
								r_Printmodule_Request <= '1';
								r_Printmodule_Code_Object <= to_integer(unsigned(c_Printer_Command_Draw(r_Command_Counter)(21 downto 16)));
								r_Printmodule_X_Object <= to_integer(unsigned(c_Printer_Command_Draw(r_Command_Counter)(15 downto 7)));
								r_Printmodule_Y_Object <= to_integer(unsigned(c_Printer_Command_Draw(r_Command_Counter)(6 downto 0)));
								r_Delay_Power <= to_integer(unsigned(c_Printer_Command_Draw(r_Command_Counter)(26 downto 22)));
							else
								r_Printmodule_Request <= '0';
								r_Command_Counter <= 0;
								r_DelayStart <= '0';
								r_Flag_2 <= '1';
							end if;

					end if;	
				elsif (i_PrintModule_State = Game_Start and i_PrintModule_Turn = "10") then 
					if r_DelayOk = '0' and r_DelayStart = '0' and r_Flag_3 = '0' then
						r_Printmodule_Request <= '1';
						r_Printmodule_Code_Object <= to_integer(unsigned(c_Printer_Command_Mario_SxtoDx(r_Command_Counter)(21 downto 16)));
						r_Printmodule_X_Object <= to_integer(unsigned(c_Printer_Command_Mario_SxtoDx(r_Command_Counter)(15 downto 7)));
						r_Printmodule_Y_Object <= to_integer(unsigned(c_Printer_Command_Mario_SxtoDx(r_Command_Counter)(6 downto 0)));
						r_Delay_Power <= 11;
						r_DelayStart <= '1';
						r_Command_Counter <= 0;
					elsif r_DelayStart = '1' and r_DelayOk = '0' and r_Flag_3 = '0' then
						r_Printmodule_Request <= '0';
					elsif r_DelayOk = '1' and r_Flag_3 = '0' then
						r_Command_Counter <= r_Command_Counter + 1;
						
						if r_Command_Counter < (c_Printer_Command_Mario_SxtoDx'HIGH + 1) then
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= to_integer(unsigned(c_Printer_Command_Mario_SxtoDx(r_Command_Counter)(21 downto 16)));
							r_Printmodule_X_Object <= to_integer(unsigned(c_Printer_Command_Mario_SxtoDx(r_Command_Counter)(15 downto 7)));
							r_Printmodule_Y_Object <= to_integer(unsigned(c_Printer_Command_Mario_SxtoDx(r_Command_Counter)(6 downto 0)));
							r_Delay_Power <= to_integer(unsigned(c_Printer_Command_Mario_SxtoDx(r_Command_Counter)(26 downto 22)));
						else
							r_Printmodule_Request <= '0';
							r_Command_Counter <= 0;
							r_DelayStart <= '0';
							r_Flag_3 <= '1';
						end if;
					end if;
				elsif (i_PrintModule_State = Game_Start and i_PrintModule_Turn = "00") then 
					if r_DelayOk = '0' and r_DelayStart = '0' and r_Flag_7 = '0' then
						r_Printmodule_Request <= '1';
						r_Printmodule_Code_Object <= 61;
						r_Printmodule_X_Object <= 511;
						r_Printmodule_Y_Object <= 50;
						r_Delay_Power <= 16;
						r_DelayStart <= '1';
						r_Command_Counter <= 0;
					elsif r_DelayStart = '1' and r_DelayOk = '0' and r_Flag_7 = '0' then
						r_Printmodule_Request <= '0';
					elsif r_DelayOk = '1' and r_Flag_7 = '0' then
						r_Command_Counter <= r_Command_Counter + 1;
						
						if r_Command_Counter < (c_Printer_Command_Mario_DxtoDeal'HIGH + 1) then
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= to_integer(unsigned(c_Printer_Command_Mario_DxtoDeal(r_Command_Counter)(21 downto 16)));
							r_Printmodule_X_Object <= to_integer(unsigned(c_Printer_Command_Mario_DxtoDeal(r_Command_Counter)(15 downto 7)));
							r_Printmodule_Y_Object <= to_integer(unsigned(c_Printer_Command_Mario_DxtoDeal(r_Command_Counter)(6 downto 0)));
							r_Delay_Power <= to_integer(unsigned(c_Printer_Command_Mario_DxtoDeal(r_Command_Counter)(26 downto 22)));
						else
							r_Printmodule_Request <= '0';
							r_Command_Counter <= 0;
							r_DelayStart <= '0';
							r_Flag_7 <= '1';
						end if;
					end if;
				elsif (i_PrintModule_State = Game_Play and i_PrintModule_Turn = "01") then
					if r_DelayOk = '0' and r_DelayStart = '0' and r_Flag_4 = '0' then
						r_Printmodule_Request <= '1';
						r_Printmodule_Code_Object <= 61;
						r_Printmodule_X_Object <= 511;
						r_Printmodule_Y_Object <= 50;
						r_DelayStart <= '1';
						r_Delay_Power <= c_Clean_Delay;
						r_Command_Counter <= 0;
					elsif r_DelayStart = '1' and r_DelayOk = '0' and r_Flag_4 = '0' then
						r_Printmodule_Request <= '0';
					elsif r_DelayOk = '1' and r_Flag_4 = '0' then
						r_Command_Counter <= r_Command_Counter + 1;
						
						if r_Command_Counter < (c_Printer_Command_Mario_DealtoSx'HIGH + 1) then
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= to_integer(unsigned(c_Printer_Command_Mario_DealtoSx(r_Command_Counter)(21 downto 16)));
							r_Printmodule_X_Object <= to_integer(unsigned(c_Printer_Command_Mario_DealtoSx(r_Command_Counter)(15 downto 7)));
							r_Printmodule_Y_Object <= to_integer(unsigned(c_Printer_Command_Mario_DealtoSx(r_Command_Counter)(6 downto 0)));
							r_Delay_Power <= to_integer(unsigned(c_Printer_Command_Mario_DealtoSx(r_Command_Counter)(26 downto 22)));
						else
							r_Printmodule_Request <= '0';
							r_Command_Counter <= 0;
							r_DelayStart <= '0';
							r_Flag_4 <= '1';
						end if;
					end if;
				elsif (i_PrintModule_State = Game_Play and i_PrintModule_Turn = "10") then 
					if r_DelayOk = '0' and r_DelayStart = '0' and r_Flag_5 = '0' then
						r_Printmodule_Request <= '0';
						r_Printmodule_Code_Object <= 62;
						r_Printmodule_X_Object <= 0;
						r_Printmodule_Y_Object <= 0;
						r_DelayStart <= '1';
						r_Delay_Power <= 5;
						r_Command_Counter <= 0;
					elsif r_DelayStart = '1' and r_DelayOk = '0' and r_Flag_5 = '0' then
						r_Printmodule_Request <= '0';
					elsif r_DelayOk = '1' and r_Flag_5 = '0' then
						r_Command_Counter <= r_Command_Counter + 1;
						
						if r_Command_Counter < (c_Printer_Command_Mario_SxtoDx'HIGH + 1) then
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= to_integer(unsigned(c_Printer_Command_Mario_SxtoDx(r_Command_Counter)(21 downto 16)));
							r_Printmodule_X_Object <= to_integer(unsigned(c_Printer_Command_Mario_SxtoDx(r_Command_Counter)(15 downto 7)));
							r_Printmodule_Y_Object <= to_integer(unsigned(c_Printer_Command_Mario_SxtoDx(r_Command_Counter)(6 downto 0)));
							r_Delay_Power <= to_integer(unsigned(c_Printer_Command_Mario_SxtoDx(r_Command_Counter)(26 downto 22)));
						else
							r_Printmodule_Request <= '0';
							r_Command_Counter <= 0;
							r_DelayStart <= '0';
							r_Flag_5 <= '1';
						end if;
					end if;
				elsif (i_PrintModule_State = Game_Play and i_PrintModule_Turn = "00") then 
					if r_DelayOk = '0' and r_DelayStart = '0' and r_Flag_8 = '0' then
						r_Printmodule_Request <= '1';
						r_Printmodule_Code_Object <= 61;
						r_Printmodule_X_Object <= 511;
						r_Printmodule_Y_Object <= 50;
						r_Delay_Power <= 16;
						r_DelayStart <= '1';
						r_Command_Counter <= 0;
					elsif r_DelayStart = '1' and r_DelayOk = '0' and r_Flag_8 = '0' then
						r_Printmodule_Request <= '0';
					elsif r_DelayOk = '1' and r_Flag_8 = '0' then
						r_Command_Counter <= r_Command_Counter + 1;
						
						if r_Command_Counter < (c_Printer_Command_Mario_DxtoDeal'HIGH + 1) then
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= to_integer(unsigned(c_Printer_Command_Mario_DxtoDeal(r_Command_Counter)(21 downto 16)));
							r_Printmodule_X_Object <= to_integer(unsigned(c_Printer_Command_Mario_DxtoDeal(r_Command_Counter)(15 downto 7)));
							r_Printmodule_Y_Object <= to_integer(unsigned(c_Printer_Command_Mario_DxtoDeal(r_Command_Counter)(6 downto 0)));
							r_Delay_Power <= to_integer(unsigned(c_Printer_Command_Mario_DxtoDeal(r_Command_Counter)(26 downto 22)));
						else
							r_Printmodule_Request <= '0';
							r_Command_Counter <= 0;
							r_DelayStart <= '0';
							r_Flag_8 <= '1';
						end if;
					end if;
				elsif (i_PrintModule_State = End_Game and r_Flag_6 = '0') then
					if r_DelayOk = '0' and r_DelayStart = '0' and r_Flag_6 = '0' then
						r_Printmodule_Request <= '1';
						r_Printmodule_Code_Object <= 61;
						r_Printmodule_X_Object <= 511;
						r_Printmodule_Y_Object <= 50;
						r_DelayStart <= '1';
						r_Delay_Power <= c_Clean_Delay;
						r_Command_Counter <= 0;
					
					elsif r_DelayStart = '1' and r_DelayOk = '0' and r_Flag_6 = '0' then
						r_Printmodule_Request <= '0';
					
					elsif r_DelayOk = '1' and r_Flag_6 = '0' then
							r_Command_Counter <= r_Command_Counter + 1;
							
							if r_Command_Counter < (c_Printer_Command_WinnerIs'HIGH + 1) then
								r_Printmodule_Request <= '1';
								r_Printmodule_Code_Object <= to_integer(unsigned(c_Printer_Command_WinnerIs(r_Command_Counter)(21 downto 16)));
								r_Printmodule_X_Object <= to_integer(unsigned(c_Printer_Command_WinnerIs(r_Command_Counter)(15 downto 7)));
								r_Printmodule_Y_Object <= to_integer(unsigned(c_Printer_Command_WinnerIs(r_Command_Counter)(6 downto 0)));
								r_Delay_Power <= to_integer(unsigned(c_Printer_Command_WinnerIs(r_Command_Counter)(26 downto 22)));
							else
								r_Printmodule_Request <= '0';
								r_Command_Counter <= 0;
								r_DelayStart <= '0';
								r_Flag_6 <= '1';
							end if;
					end if;
				elsif (i_PrintModule_State = End_Game and r_Flag_6 = '1') then
					if r_DelayOk = '0' and r_DelayStart = '0' and r_Flag_9 = '0' then
						r_Printmodule_Request <= '1';
						r_Printmodule_Code_Object <= 44;
						r_Printmodule_X_Object <= 262;
						r_Printmodule_Y_Object <= 25;
						r_DelayStart <= '1';
						r_Delay_Power <= 15;
						r_Command_Counter <= 0;
				
					elsif r_DelayStart = '1' and r_DelayOk = '0' and r_Flag_9 = '0' then
						r_Printmodule_Request <= '0';
					
					elsif r_DelayOk = '1' and r_Flag_9 = '0' then
							r_Command_Counter <= r_Command_Counter + 1;
						if r_Command_Counter < (c_Printer_Command_Player1'HIGH + 1) then
							r_Printmodule_Request <= '1';
							if (i_Ps_Player1_Sum > i_Ps_Player2_Sum and i_Ps_Player1_Sum > i_Ps_Dealer_Sum) then
								r_Printmodule_Code_Object <= to_integer(unsigned(c_Printer_Command_Player1(r_Command_Counter)(21 downto 16)));
								r_Printmodule_X_Object <= to_integer(unsigned(c_Printer_Command_Player1(r_Command_Counter)(15 downto 7)));
								r_Printmodule_Y_Object <= to_integer(unsigned(c_Printer_Command_Player1(r_Command_Counter)(6 downto 0)));
								r_Delay_Power <= to_integer(unsigned(c_Printer_Command_Player1(r_Command_Counter)(26 downto 22)));
							elsif (i_Ps_Player2_Sum > i_Ps_Player1_Sum and i_Ps_Player2_Sum > i_Ps_Dealer_Sum) then
								r_Printmodule_Code_Object <= to_integer(unsigned(c_Printer_Command_Player2(r_Command_Counter)(21 downto 16)));
								r_Printmodule_X_Object <= to_integer(unsigned(c_Printer_Command_Player2(r_Command_Counter)(15 downto 7)));
								r_Printmodule_Y_Object <= to_integer(unsigned(c_Printer_Command_Player2(r_Command_Counter)(6 downto 0)));
								r_Delay_Power <= to_integer(unsigned(c_Printer_Command_Player2(r_Command_Counter)(26 downto 22)));
							elsif (i_Ps_Player1_Sum = i_Ps_Player2_Sum and i_Ps_Player1_Sum > i_Ps_Dealer_Sum) then
								r_Printmodule_Code_Object <= to_integer(unsigned(c_Printer_Command_P1andP2(r_Command_Counter)(21 downto 16)));
								r_Printmodule_X_Object <= to_integer(unsigned(c_Printer_Command_P1andP2(r_Command_Counter)(15 downto 7)));
								r_Printmodule_Y_Object <= to_integer(unsigned(c_Printer_Command_P1andP2(r_Command_Counter)(6 downto 0)));
								r_Delay_Power <= to_integer(unsigned(c_Printer_Command_P1andP2(r_Command_Counter)(26 downto 22)));
							elsif (i_Ps_Dealer_Sum >= i_Ps_Player1_Sum and i_Ps_Dealer_Sum >= i_Ps_Player2_Sum) then
								r_Printmodule_Code_Object <= to_integer(unsigned(c_Printer_Command_Dealer(r_Command_Counter)(21 downto 16)));
								r_Printmodule_X_Object <= to_integer(unsigned(c_Printer_Command_Dealer(r_Command_Counter)(15 downto 7)));
								r_Printmodule_Y_Object <= to_integer(unsigned(c_Printer_Command_Dealer(r_Command_Counter)(6 downto 0)));
								r_Delay_Power <= to_integer(unsigned(c_Printer_Command_Dealer(r_Command_Counter)(26 downto 22)));			
							end if ;
						else
							r_Printmodule_Request <= '0';
							r_Command_Counter <= 0;
							r_DelayStart <= '0';
							r_Flag_9 <= '1';
						end if ;
					end if;	
				end if ;
			end if ;
		end if ;
	end process;
	
	o_Printmodule_Request <= r_Printmodule_Request;
	o_Printmodule_Code_Object <= r_Printmodule_Code_Object;
	o_Printmodule_X_Object <= r_Printmodule_X_Object;
	o_Printmodule_Y_Object <= r_Printmodule_Y_Object;

end Behavioral;

