library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Constants.ALL;

entity Master_Module is
port(
	i_Master_Clk : in std_logic;
	i_Master_On : in std_logic;
	i_Master_Buttons	: in std_logic_vector(2 downto 0);

	i_Master_Players_Buttons	: in std_logic_vector (3 downto 0);
	i_Master_Music_On  : in std_logic;

	o_Master_Led	: out std_logic_vector(3 downto 0);
	o_Master_R 		: out std_logic_vector(4 downto 0);
	o_Master_G 		: out std_logic_vector(5 downto 0);
	o_Master_B 		: out std_logic_vector(4 downto 0);
	
	o_Master_HSync 	: out std_logic;
	o_Master_VSync 	: out std_logic;

	o_Master_Lcd_Data_1 : out std_logic_vector(7 downto 0);
	o_Master_Lcd_Control_1 : out std_logic_vector(6 downto 4);

	o_Master_Lcd_Data_2 : out std_logic_vector(7 downto 0);
	o_Master_Lcd_Control_2 : out std_logic_vector(6 downto 4);

	o_Master_Music_Out : out std_logic
	);
end Master_Module;

architecture Behavioral of Master_Module is

	Component Clock_Regulator is
	port (
	  	i_CR_Clk           : in     std_logic;
		o_CR_Clk_125          : out    std_logic;
		o_CR_Clk_40          : out    std_logic;
		i_CR_Reset             : in     std_logic
	 );
	end Component Clock_Regulator;

	Component Control_Unit is
	port (
		i_CU_Clk		: in std_logic;
		i_CU_On 		: in std_logic;
		--i_CU_EndGame	: in std_logic;
		i_CU_Buttons 	: in std_logic_vector(2 downto 0);

		i_CU_P1_Buttons	: in std_logic_vector (1 downto 0);
		i_CU_P2_Buttons	: in std_logic_vector (1 downto 0);

		o_CU_Reset		: out std_logic;
		o_CU_FSM_State 	: out FSM_State_Main;
		o_CU_EndGame	: out std_logic;
		o_CU_Turn		: out std_logic_vector (1 downto 0);
		o_CU_Buttons	: out std_logic_vector (2 downto 0);
		o_CU_Player_Buttons	: out std_logic_vector (1 downto 0)
		--o_CU_Led		: out std_logic_vector (3 downto 0);
		--o_Turn_Led: out std_logic_vector(3 downto 0)

		);
	end Component Control_Unit;

	Component Vga is
	port (
		i_Vga_Clk_125 		: in  STD_LOGIC;
		i_Vga_Clk_40 		: in  STD_LOGIC;

		i_Vga_Reset 		: in std_logic;
		i_Vga_FSM_State 	: in FSM_State_Main;
		i_Vga_Turn			: in std_logic_vector (1 downto 0);
		
		i_Vga_Dealer_Sum	: in integer range 0 to 63;	
		i_Vga_Player1_Sum	: in integer range 0 to 63;
		i_Vga_Player2_Sum	: in integer range 0 to 63;

		
		i_Vga_Vram_Select	: in std_logic_vector(1 downto 0);
		i_Vga_Vram_Data 	: in std_logic_vector(2 downto 0);
		i_Vga_Vram_Address 	: in std_logic_vector(15 downto 0);

		o_Vga_R 		: out std_logic_vector(4 downto 0);
		o_Vga_G 		: out std_logic_vector(5 downto 0);
		o_Vga_B 		: out std_logic_vector(4 downto 0);
		
		o_Vga_HSync 	: out std_logic;
		o_Vga_VSync 	: out std_logic
		);
	end Component Vga;


	Component Lcd_Manager is
	port(
		i_LcdManager_Clk 		: in std_logic;
		i_LcdManager_FSM_State 	: in FSM_State_Main;

		--i_LcdManager_Switch_1	: in std_logic;
		--i_LcdManager_Selector 	: in std_logic_vector(1 downto 0);

		i_LcdManager_Turn 		: in std_logic_vector(1 downto 0);

		i_LcdManager_P1_Card1	: in std_logic_vector (5 downto 0);
		i_LcdManager_P1_Card2	: in std_logic_vector (5 downto 0);
		i_LcdManager_P2_Card1	: in std_logic_vector (5 downto 0);
		i_LcdManager_P2_Card2	: in std_logic_vector (5 downto 0);

		o_LcdManager_Data_1		: out std_logic_vector(7 downto 0);		--output bus, used for data transfer (DB)
		o_LcdManager_Control_1	: out std_logic_vector (6 downto 4);

		o_LcdManager_Data_2		: out std_logic_vector(7 downto 0);		--output bus, used for data transfer (DB)
		o_LcdManager_Control_2	: out std_logic_vector (6 downto 4)

		);
	end Component Lcd_Manager;

	Component Players_Manager is
	port(
		i_Players_Clk 		: in  STD_LOGIC;
		i_Players_Reset 	: in std_logic;
		i_Players_Status	: in FSM_State_Main;

		i_Players_Hit		: in std_logic;
		i_Players_Turn		: in std_logic_vector(1 downto 0);
		i_Players_EndGame	: in std_logic;
		i_Players_Stand		: in std_logic;

		o_Players_Vram_Sel		: out std_logic_vector(1 downto 0);
		o_Players_Vram_Data 	: out std_logic_vector(2 downto 0);
		o_Players_Vram_Address 	: out std_logic_vector(15 downto 0);

		o_Players_LCD1_Card1		: out std_logic_vector (5 downto 0);
		o_Players_LCD1_Card2		: out std_logic_vector (5 downto 0);
		o_Players_LCD2_Card1		: out std_logic_vector (5 downto 0);
		o_Players_LCD2_Card2		: out std_logic_vector (5 downto 0);


		o_Players_Dealer_Sum			: out integer range 0 to 63;
		o_Players_Player1_Sum			: out integer range 0 to 63;
		o_Players_Player2_Sum			: out integer range 0 to 63

	);
	end Component Players_Manager;

	Component Music_Module is
	port(
		i_Music_Clk		: in std_logic;
		i_Music_On  : in std_logic;
		i_Music_FSM_State 	: in FSM_State_Main;
		i_Music_Dealer_Sum	: in integer range 0 to 63;	
		i_Music_Player1_Sum	: in integer range 0 to 63;
		i_Music_Player2_Sum	: in integer range 0 to 63;
		o_Music_Out 		: out std_logic
		);
	end Component Music_Module;
	
  	signal r_Clock_125, r_Clock_40, r_Reset, r_EndGame : std_logic;

	signal r_Players_Vram_Sel		: std_logic_vector(1 downto 0);
	signal r_Players_Vram_Data		: std_logic_vector(2 downto 0);
	signal r_Players_Vram_Address 	: std_logic_vector(15 downto 0);

	signal r_Players_Enabled	: std_logic_vector(1 downto 0);

	SIGNAL FSM_State : FSM_State_Main;
  	signal r_Buttons, r_Switches : std_logic_vector (2 downto 0);
	
	signal r_Players_LCD1_Card1, r_Players_LCD1_Card2, r_Players_LCD2_Card1, r_Players_LCD2_Card2 : std_logic_vector (5 downto 0);
	signal r_Ps_Dealer_Sum, r_Ps_Player1_Sum, r_Ps_Player2_Sum : integer range 0 to 63;

	signal r_Lcd_selector :std_logic_vector (1 downto 0);
begin
	--r_Players_Enabled <= --r_Switches(1) & r_Switches(2);

	CR : Clock_Regulator port map (i_Master_Clk, r_Clock_125, r_Clock_40, '0');

	--IM : Input_Manager port map (r_Clock_125, i_Master_Buttons, r_Buttons_Stable);

	CU : Control_Unit
	port map (
		r_Clock_125,
		i_Master_On,
		--i_Master_Switch,
		i_Master_Buttons,
		i_Master_Players_Buttons(1 downto 0),
		i_Master_Players_Buttons(3 downto 2),
		r_Reset,
		FSM_State,
		r_EndGame,
		r_Players_Enabled,
		r_Buttons,
		o_Master_Led(1 downto 0)
		--open,--o_Vga_Led,
		--open--o_Vga_Led
		);

	Vga_Mod : Vga
	port map (
		r_Clock_125,
		r_Clock_40,
		'0',
		FSM_State,
		r_Players_Enabled,
		r_Ps_Dealer_Sum,
		r_Ps_Player1_Sum,
		r_Ps_Player2_Sum,
		r_Players_Vram_Sel,
		r_Players_Vram_Data,
		r_Players_Vram_Address,
		o_Master_R,
		o_Master_G,
		o_Master_B,
		o_Master_HSync,
		o_Master_VSync);
		--o_Vga_Led);

	Players_Mod : Players_Manager
	port map(
		r_Clock_125,
		r_Reset,
		FSM_State,
		r_Buttons(0),
		r_Players_Enabled,
		r_EndGame,
		'0',--r_Buttons(1),
		r_Players_Vram_Sel,
		r_Players_Vram_Data,
		r_Players_Vram_Address,
		r_Players_LCD1_Card1,
		r_Players_LCD1_Card2,
		r_Players_LCD2_Card1,
		r_Players_LCD2_Card2,
		r_Ps_Dealer_Sum,
		r_Ps_Player1_Sum,
		r_Ps_Player2_Sum
		);

	Lcd_Man : Lcd_Manager
	port map (
		r_Clock_125,
		FSM_State,
		--i_Master_Switch_1,
		--r_Lcd_selector,
		r_Players_Enabled,
		r_Players_LCD1_Card1,
		r_Players_LCD1_Card2,
		r_Players_LCD2_Card1,
		r_Players_LCD2_Card2,
		o_Master_Lcd_Data_1,
		o_Master_Lcd_Control_1,
		o_Master_Lcd_Data_2,
		o_Master_Lcd_Control_2

		);		

	Music : Music_Module
	port map(
		r_Clock_125,
		i_Master_Music_On,
		FSM_State,
		r_Ps_Dealer_Sum,
		r_Ps_Player1_Sum,
		r_Ps_Player2_Sum,
		o_Master_Music_Out
		);

end Behavioral;