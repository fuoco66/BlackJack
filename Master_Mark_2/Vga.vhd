library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Constants.ALL;
--use IEEE.NUMERIC_STD.ALL;

entity Vga is
PORT (
	i_Vga_Clk_125 		: in  STD_LOGIC;
	i_Vga_Clk_40 		: in  STD_LOGIC;

	i_Vga_Reset 		: in std_logic;
	i_Vga_FSM_State 	: in FSM_State_Main;
	i_Vga_Turn			: in std_logic_vector (1 downto 0);
	
	i_Vga_Dealer_Sum	: in integer range 0 to 63;	
	i_Vga_Player1_Sum	: in integer range 0 to 63;
	i_Vga_Player2_Sum	: in integer range 0 to 63;

	
	i_Vga_Vram_Select	: in std_logic_vector(1 downto 0);
	i_Vga_Vram_Data 	: in std_logic_vector(2 downto 0);
	i_Vga_Vram_Address 	: in std_logic_vector(15 downto 0);

	o_Vga_R 		: out std_logic_vector(4 downto 0);
	o_Vga_G 		: out std_logic_vector(5 downto 0);
	o_Vga_B 		: out std_logic_vector(4 downto 0);
	
	o_Vga_HSync 	: out std_logic;
	o_Vga_VSync 	: out std_logic
	--o_Vga_Led: out std_logic_vector(3 downto 0)

);
end Vga;

architecture Behavioral of Vga is

	Component Synchronizer is
	PORT (
		i_Sync_Clk 			: in  std_logic;
		i_Sync_Reset		: in std_logic;
		o_Sync_HSync 		: out  std_logic;
	    o_Sync_VSync 		: out  std_logic;
	    
	    o_Sync_ActiveVRam 	: out std_logic_vector(2 downto 0);
	    o_Sync_VideoOn 		: out std_logic -- tocheck
	);
	end Component Synchronizer;

	Component Drawer is
	PORT (
		i_Draw_VgaClk 			: in std_logic;
		i_Draw_Reset 			: in std_logic;
		
		i_Draw_VRam_Data_Dealer 	: in std_logic_vector(2 downto 0);
		o_Draw_VRam_Address_Dealer	: out std_logic_vector(15 downto 0);

		i_Draw_VRam_Data_1 			: in std_logic_vector(2 downto 0);
		o_Draw_VRam_Address_1		: out std_logic_vector(15 downto 0);

		i_Draw_VRam_Data_2 			: in std_logic_vector(2 downto 0);
		o_Draw_VRam_Address_2		: out std_logic_vector(15 downto 0);

		i_Draw_VRam_Data_Comm 		: in std_logic_vector(2 downto 0);
		o_Draw_VRam_Address_Comm	: out std_logic_vector(15 downto 0);

		i_Draw_ActiveVRam 			: in std_logic_vector(2 downto 0);
		i_Draw_VideoOn 				: in std_logic;
		
		o_Draw_RGB 					: out std_logic_vector(2 downto 0)
		);

	end Component Drawer;

	Component Memory_Cleaner is
	port (
		i_MC_Clk_125 		: in  std_logic;
		i_MC_FSM_State 		: in FSM_State_Vga;

		o_MC_VRam_Enable	: out std_logic;
		o_MC_VRam_Data 		: out std_logic_vector(2 downto 0);
		o_MC_VRam_Address	: out std_logic_vector(15 downto 0)
		);
	end Component Memory_Cleaner;

	Component VRam_3Bit IS
	  PORT (
	    clka : IN STD_LOGIC;
	    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
	    addra : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
	    dina : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
	    clkb : IN STD_LOGIC;
	    addrb : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
	    doutb : OUT STD_LOGIC_VECTOR(2 DOWNTO 0)
	  );
	end Component VRam_3Bit;

	Component Comm_Ram_Manager is
	port(
		i_Master_Clk : in std_logic;
		i_Master_On : in std_logic;

		i_Master_Status 		: FSM_state_Main;
		i_Master_Turn			: std_logic_vector (1 downto 0);
		
		i_Master_Dealer_Sum	: integer range 0 to 63;	
		i_Master_Player1_Sum	: integer range 0 to 63;
		i_Master_Player2_Sum	: integer range 0 to 63;


		o_Master_VRam_Select	: out std_logic_vector (0 downto 0);
		o_Master_VRam_Address	: out std_logic_vector (15 downto 0);
		o_Master_VRam_Data 	: out std_logic_vector (2 downto 0)
		);
	end Component;

	signal r_Clk_125, r_Clk_40, r_VideoOn, r_Reset, r_Printer_Reset : std_logic; 
	signal r_ActiveRam, r_Vram_Data_D, r_Vram_Data_1, r_Vram_Data_2, r_Vram_Data_Comm, r_RGB : std_logic_vector(2 downto 0);

	signal r_Vram_Address_D, r_Vram_Address_1, r_Vram_Address_2, r_Vram_Address_Comm : std_logic_vector(15 downto 0);
	signal r_Vram_Address_FlyBy_D, r_Vram_Address_FlyBy_1, r_Vram_Address_FlyBy_2, r_Vram_Address_FlyBy_Comm : std_logic_vector(15 downto 0);



	signal r_Vram_Write_Address_1, r_Vram_Write_Address_2, r_Vram_Write_Address_3, r_Vram_Write_Address_Comm 	: std_logic_vector(15 downto 0);
	signal r_Vram_Write_Data_1, r_Vram_Write_Data_2, r_Vram_Write_Data_3, r_Vram_Write_Data_Comm		: std_logic_vector(2 downto 0);

	signal r_Vram_MemoryCleaner_Address_1, r_Vram_MemoryCleaner_Address_2, r_Vram_MemoryCleaner_Address_3 	: std_logic_vector(15 downto 0);
	signal r_Vram_MemoryCleaner_Data_1, r_Vram_MemoryCleaner_Data_2, r_Vram_MemoryCleaner_Data_3			: std_logic_vector(2 downto 0);

	signal r_Write_Enabled : std_logic_vector(2 downto 0);

	signal r_Write_Enabled_Dealer, r_Write_Enabled_P1, r_Write_Enabled_P2, r_Write_Enabled_Comm : std_logic_vector(0 downto 0);
	signal r_MemoryCleaner_Enabled_1, r_MemoryCleaner_Enabled_2, r_MemoryCleaner_Enabled_3 : std_logic;
	signal Current_State, Next_State : FSM_State_Vga;

begin

	--r_Vram_Write_Data <= i_Vga_Vram_Data;
	--r_Vram_Write_Address <= i_Vga_Vram_Address;

	--r_Write_Enabled(0) <= (not i_Vga_Vram_Select(1)) 	and (not i_Vga_Vram_Select(0)); -- Dealer
	--r_Write_Enabled(1) <= (not i_Vga_Vram_Select(1))	and (i_Vga_Vram_Select(0));		-- Player_1
	--r_Write_Enabled(2) <= (i_Vga_Vram_Select(1)) 		and (not i_Vga_Vram_Select(0)); -- Player_2

	r_Write_Enabled_Dealer(0) 	<= r_Write_Enabled(0);
	r_Write_Enabled_P1(0) 		<= r_Write_Enabled(1);
	r_Write_Enabled_P2(0) 		<= r_Write_Enabled(2);

	Printer_Comm : Comm_Ram_Manager port map(i_Vga_Clk_125, r_Printer_Reset, i_Vga_FSM_State, i_Vga_Turn, i_Vga_Dealer_Sum, i_Vga_Player1_Sum, i_Vga_Player2_Sum, r_Write_Enabled_Comm, r_Vram_Write_Address_Comm, r_Vram_Write_Data_Comm);

	Syncro 	: Synchronizer port map(i_Vga_Clk_40, r_Reset, o_Vga_HSync, o_Vga_VSync, r_ActiveRam, r_VideoOn);

	Vram_D 		: VRam_3Bit port map(i_Vga_Clk_125, r_Write_Enabled_Dealer, r_Vram_Write_Address_1, r_Vram_Write_Data_1, i_Vga_Clk_40, r_Vram_Address_D, r_Vram_Data_D);

	Vram_P1 	: VRam_3Bit port map(i_Vga_Clk_125, r_Write_Enabled_P1, r_Vram_Write_Address_2, r_Vram_Write_Data_2, i_Vga_Clk_40, r_Vram_Address_1, r_Vram_Data_1);

	Vram_P2 	: VRam_3Bit port map(i_Vga_Clk_125, r_Write_Enabled_P2, r_Vram_Write_Address_3, r_Vram_Write_Data_3, i_Vga_Clk_40, r_Vram_Address_2, r_Vram_Data_2);

	--Vram_Comm	: VRam_3Bit port map(i_Vga_Clk_125, r_Write_Enabled_Dealer, r_Vram_Write_Address_1, r_Vram_Write_Data_1, i_Vga_Clk_40, r_Vram_Address_Comm, r_Vram_Data_Comm);
	Vram_Comm	: VRam_3Bit port map(i_Vga_Clk_125, r_Write_Enabled_Comm, r_Vram_Write_Address_Comm, r_Vram_Write_Data_Comm, i_Vga_Clk_40, r_Vram_Address_Comm, r_Vram_Data_Comm);

	Draw 	: Drawer port map
	(
		i_Vga_Clk_40,
		r_Reset,
		r_Vram_Data_D,
		r_Vram_Address_FlyBy_D,
		r_Vram_Data_1,
		r_Vram_Address_FlyBy_1,
		r_Vram_Data_2,
		r_Vram_Address_FlyBy_2,
		r_Vram_Data_Comm,
		r_Vram_Address_FlyBy_Comm,
		r_ActiveRam,
		r_VideoOn,
		r_RGB

		);

	MC_1 : Memory_Cleaner port map
	(
		i_Vga_Clk_125,
		Current_State,
		r_MemoryCleaner_Enabled_1,
		r_Vram_MemoryCleaner_Data_1,
		r_Vram_MemoryCleaner_Address_1
		);

	MC_2 : Memory_Cleaner port map
	(
		i_Vga_Clk_125,
		Current_State,
		r_MemoryCleaner_Enabled_2,
		r_Vram_MemoryCleaner_Data_2,
		r_Vram_MemoryCleaner_Address_2
		);

	MC_3 : Memory_Cleaner port map
	(
		i_Vga_Clk_125,
		Current_State,
		r_MemoryCleaner_Enabled_3,
		r_Vram_MemoryCleaner_Data_3,
		r_Vram_MemoryCleaner_Address_3
		);

	o_Vga_R 	<= ( others => r_RGB(2) );
	o_Vga_G 	<= ( others => r_RGB(1) );
	o_Vga_B 	<= ( others => r_RGB(0) );


	Current : process (i_Vga_Clk_125, Next_State, i_Vga_FSM_State)
	begin
		if i_Vga_Clk_125 = '1' and i_Vga_Clk_125'Event then
			if i_Vga_FSM_State /= Reset then 
				Current_State <= Next_State;
			else
				Current_State <= Reset_Vga;
			end if;
		end if;
	end process;

	nextP : process (Current_State, i_Vga_FSM_State)
	begin
		case Current_State is
			when Reset_Vga =>
				if i_Vga_FSM_State = Reset then 
					Next_State <= Reset_Vga;
				else
					Next_State <= New_Game_Vga;
				end if;
			when New_Game_Vga =>
				
				if i_Vga_FSM_State = Reset_Delay or i_Vga_FSM_State = New_Game then 
					Next_State <= New_Game_Vga;
				else
					Next_State <= Idle_Vga;
				end if;
			
			when Idle_Vga =>	--punto di partenza!!!!!!
				if i_Vga_FSM_State = Reset then
					Next_State <= Reset_Vga;
				elsif i_Vga_FSM_State = New_Game then
					Next_State <= New_Game_Vga;					
				else
					Next_State <= Idle_Vga;
				end if;
		end case;
	end process;

	output : process (Current_State)
		begin   
			case Current_State is
				when Reset_Vga =>
					r_Reset 	<= '1';
					r_Printer_Reset <= '1';
					--o_Vga_Led 	<= "0111";

					r_Vram_Write_Data_1 		<= (others => '0');
					r_Vram_Write_Address_1 	<= (others => '0');

					r_Vram_Write_Data_2 		<= (others => '0');
					r_Vram_Write_Address_2 	<= (others => '0');

					r_Vram_Write_Data_3 		<= (others => '0');
					r_Vram_Write_Address_3 	<= (others => '0');


					r_Write_Enabled(0) 		<= '0'; -- Dealer
					r_Write_Enabled(1) 		<= '0';	-- Player_1
					r_Write_Enabled(2) 		<= '0'; -- Player_2

					r_Vram_Address_D <= "1111111111111110";
					r_Vram_Address_1 <= "1111111111111110";
					r_Vram_Address_2 <= "1111111111111110";


				when New_Game_Vga =>
					r_Reset <= '0'; -- to check
					r_Printer_Reset <= '1';
					--o_Vga_Led <= "1011";

					r_Vram_Write_Data_1 		<= r_Vram_MemoryCleaner_Data_1;
					r_Vram_Write_Address_1 	<= r_Vram_MemoryCleaner_Address_1;

					r_Vram_Write_Data_2 		<= r_Vram_MemoryCleaner_Data_2;
					r_Vram_Write_Address_2 	<= r_Vram_MemoryCleaner_Address_2;

					r_Vram_Write_Data_3 		<= r_Vram_MemoryCleaner_Data_3;
					r_Vram_Write_Address_3 	<= r_Vram_MemoryCleaner_Address_3;

					r_Write_Enabled(0) 		<= r_MemoryCleaner_Enabled_1;--r_MemoryCleaner_Enabled; -- Dealer
					r_Write_Enabled(1) 		<= r_MemoryCleaner_Enabled_2;--r_MemoryCleaner_Enabled;		-- Player_1
					r_Write_Enabled(2) 		<= r_MemoryCleaner_Enabled_3;--r_MemoryCleaner_Enabled; -- Player_2

					r_Vram_Address_D <= "1111111111111110";
					r_Vram_Address_1 <= "1111111111111110";
					r_Vram_Address_2 <= "1111111111111110";

				when Idle_Vga =>
					r_Reset <= '0';
					r_Printer_Reset <= '0';

					--o_Vga_Led <= "1101";

					r_Vram_Write_Data_1 		<= i_Vga_Vram_Data;
					r_Vram_Write_Address_1	<= i_Vga_Vram_Address;

					r_Vram_Write_Data_2 		<= i_Vga_Vram_Data;
					r_Vram_Write_Address_2	<= i_Vga_Vram_Address;

					r_Vram_Write_Data_3 		<= i_Vga_Vram_Data;
					r_Vram_Write_Address_3	<= i_Vga_Vram_Address;

					r_Write_Enabled(0) 		<= (not i_Vga_Vram_Select(1)) 	and (not i_Vga_Vram_Select(0)); -- Dealer
					r_Write_Enabled(1) 		<= (not i_Vga_Vram_Select(1))	and (i_Vga_Vram_Select(0));		-- Player_1
					r_Write_Enabled(2) 		<= (i_Vga_Vram_Select(1)) 		and (not i_Vga_Vram_Select(0)); -- Player_2

					r_Vram_Address_D <= r_Vram_Address_FlyBy_D;
					r_Vram_Address_1 <= r_Vram_Address_FlyBy_1;
					r_Vram_Address_2 <= r_Vram_Address_FlyBy_2;
					r_Vram_Address_Comm <= r_Vram_Address_FlyBy_Comm;

			end case;	
		end process;




end Behavioral;

