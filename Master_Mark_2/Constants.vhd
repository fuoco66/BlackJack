
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package Constants is
	
	--Main FSM 
	TYPE FSM_State_Main IS (Reset, Reset_Delay, Main_Init, New_Game, Game_Start, Game_Play, Game_Finish, End_Game, Idle);  -- Rename New game in soft-reset
	
	constant c_Reset_Limit 	: integer := 250000000;--65663;--65535;-- 40001;-- 65530;
	constant c_Main_Init_Limit 	: integer := 625000000;		--5 s
	constant c_New_Game_Limit 	: integer := 65530;			--0,52 ms
	constant c_End_Game_Limit 	: integer := 750000000;--500000000;--375000000;--250000000;		--2s
	constant c_Game_Finish_Limit 	: integer := 125000000; -- 1s
	constant c_Idle_Limit 	: integer := 0;

	--constant c_Reset_Limit 	: integer := 40001;--65663;--65535;-- 65530;--
	--constant c_Main_Init_Limit 	: integer := 40001;--625000000;
	--constant c_New_Game_Limit 	: integer := 40001;--65530;--65535;--
	--constant c_End_Game_Limit 	: integer := 40001;--625000000;
	--constant c_Idle_Limit 	: integer := 0;


	constant c_DEBOUNCE_LIMIT : integer := 7812500;--15625000;--31250000;--2500000;--1250000;--5;--

	--constant c_Reset_Limit 	: integer := 125000000; --1s
	--constant c_NewGame_Limit 	: integer := 125000000; --1s

	--constant c_Reset_Limit 	: integer := 250000000; --2s
	--constant c_NewGame_Limit 	: integer := 250000000; --2s

	--constant c_Reset_Limit 	: integer := 800;	 --800ns
	--constant c_NewGame_Limit 	: integer := 800;--800ns
	
	--Main FSM 
	TYPE FSM_State_Vga IS (Reset_Vga, New_Game_Vga, Idle_Vga);
	--TYPE Game_State IS (Game_Start, Game_Play, Game_Finish);

	

	--800 x 600
	constant c_H_VisibleArea 	: integer :=800;
	constant c_H_FrontPorch		: integer :=40;
	constant c_H_SyncPulse 		: integer :=128;
	constant c_H_BackPorch 		: integer :=88;
	constant c_H_Whole 			: integer :=1056;

	constant c_V_VisibleArea 	: integer :=600;
	constant c_V_FrontPorch 	: integer :=1;
	constant c_V_SyncPulse 		: integer :=5; --3
	constant c_V_BackPorch 		: integer :=23;
	constant c_V_Whole 			: integer :=628;

	constant c_VRam_Limit 				: integer := 65535;
	
	constant c_VRam_Dealer_Width 		: integer := 512;
	constant c_VRam_Dealer_Height 		: integer := 128;
	constant c_VRam_Offset_X_Dealer 	: integer := 144;--5;--
	constant c_VRam_Offset_Y_Dealer		: integer := 50;--1;--
	
	constant c_VRam_Player_Width 		: integer := 256;
	constant c_VRam_Player_Height 		: integer := 256;
	constant c_VRam_Offset_X_Player_1 	: integer := 96;
	constant c_VRam_Offset_Y_Player_1 	: integer := 320;

	constant c_VRam_Offset_X_Player_2 	: integer :=448;
	constant c_VRam_Offset_Y_Player_2 	: integer :=320;

	constant c_VRam_Offset_X_Comm		: integer :=144;
	constant c_VRam_Offset_Y_Comm 	 	: integer :=180;

end Constants;

package body Constants is
 
end Constants;


	---- 1280 x 1024
	--constant c_H_VisibleArea 	: integer :=1280;
	--constant c_H_FrontPorch		: integer :=48;
	--constant c_H_SyncPulse 		: integer :=112;
	--constant c_H_BackPorch 		: integer :=248;
	--constant c_H_Whole 			: integer :=1688;

	--constant c_V_VisibleArea 	: integer :=1024;
	--constant c_V_FrontPorch 	: integer :=1;
	--constant c_V_SyncPulse 		: integer :=4; --3
	--constant c_V_BackPorch 		: integer :=38;
	--constant c_V_Whole 			: integer :=1066;

	--constant c_VRam_Limit 				: integer :=65536;
	
	--constant c_VRam_Player_Width 		: integer :=512;
	--constant c_VRam_Player_Height 		: integer :=128;

	--constant c_VRam_Offset_X_Dealer 	: integer :=0;
	--constant c_VRam_Offset_Y_Dealer		: integer :=0;
	
	--constant c_VRam_Offset_X_Common		: integer :=0;
	--constant c_VRam_Offset_Y_Common 	: integer :=0;
	
	--constant c_VRam_Offset_X_Player_1 	: integer :=0;
	--constant c_VRam_Offset_Y_Player_1 	: integer :=400;

	--constant c_VRam_Offset_X_Player_2 	: integer :=0;
	--constant c_VRam_Offset_Y_Player_2 	: integer :=600;



	----640 x 480
	--constant c_H_VisibleArea 	: integer :=640;
	--constant c_H_FrontPorch		: integer :=16;
	--constant c_H_SyncPulse 		: integer :=96;
	--constant c_H_BackPorch 		: integer :=48;
	--constant c_H_Whole 			: integer :=800;

	--constant c_V_VisibleArea 	: integer :=480;
	--constant c_V_FrontPorch 	: integer :=10;
	--constant c_V_SyncPulse 		: integer :=3; --3
	--constant c_V_BackPorch 		: integer :=33;
	--constant c_V_Whole 			: integer :=525;

	--constant c_VRam_Limit 				: integer :=65536;
	
	--constant c_VRam_Player_Width 		: integer :=512;
	--constant c_VRam_Player_Height 		: integer :=128;

	--constant c_VRam_Offset_X_Dealer 	: integer :=0;
	--constant c_VRam_Offset_Y_Dealer		: integer :=0;
	
	--constant c_VRam_Offset_X_Common		: integer :=0;
	--constant c_VRam_Offset_Y_Common 	: integer :=0;
	
	--constant c_VRam_Offset_X_Player_1 	: integer :=0;
	--constant c_VRam_Offset_Y_Player_1 	: integer :=200;

	--constant c_VRam_Offset_X_Player_2 	: integer :=0;
	--constant c_VRam_Offset_Y_Player_2 	: integer :=400;

