library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package Music_Constants is


	type Music_Commands_Mem is array(integer range <>) of integer;--std_logic_vector(22 downto 0);

	type Music_Block is array(integer range <>) of std_logic_vector(35 downto 0);
	--type Music_Block is array(integer range <>) of std_logic_vector(7 downto 0);


--Notes
----------------------------------------------------------------------------------------------------------------
	constant c_NOTE_C0 : integer := 7812500; -- 16
	constant c_NOTE_C1 : integer := 3787879; -- 33
	constant c_NOTE_C2 : integer := 1923077; -- 65
	constant c_NOTE_C3 : integer := 954198; -- 131
	constant c_NOTE_C4 : integer := 478927; -- 261
	constant c_NOTE_C5 : integer := 239006; -- 523
	constant c_NOTE_C6 : integer := 119389; -- 1047
	constant c_NOTE_C7 : integer := 59723; -- 2093
	constant c_NOTE_C8 : integer := 29861; -- 4186
	constant c_NOTE_C9 : integer := 14931; -- 8372

	constant c_NOTE_CS0 : integer := 7352941; -- 17
	constant c_NOTE_CS1 : integer := 3571429; -- 35
	constant c_NOTE_CS2 : integer := 1811594; -- 69
	constant c_NOTE_CS3 : integer := 899281; -- 139
	constant c_NOTE_CS4 : integer := 451264; -- 277
	constant c_NOTE_CS5 : integer := 225632; -- 554
	constant c_NOTE_CS6 : integer := 112714; -- 1109
	constant c_NOTE_CS7 : integer := 56382; -- 2217
	constant c_NOTE_CS8 : integer := 28185; -- 4435
	constant c_NOTE_CS9 : integer := 14092; -- 8870

	constant c_NOTE_D0 : integer := 6944444; -- 18
	constant c_NOTE_D1 : integer := 3378378; -- 37
	constant c_NOTE_D2 : integer := 1712329; -- 73
	constant c_NOTE_D3 : integer := 850340; -- 147
	constant c_NOTE_D4 : integer := 425170; -- 294
	constant c_NOTE_D5 : integer := 212947; -- 587
	constant c_NOTE_D6 : integer := 106383; -- 1175
	constant c_NOTE_D7 : integer := 53214; -- 2349
	constant c_NOTE_D8 : integer := 26601; -- 4699
	constant c_NOTE_D9 : integer := 13302; -- 9397

	constant c_NOTE_DS0 : integer := 6578947; -- 19
	constant c_NOTE_DS1 : integer := 3205128; -- 39
	constant c_NOTE_DS2 : integer := 1602564; -- 78
	constant c_NOTE_DS3 : integer := 801282; -- 156
	constant c_NOTE_DS4 : integer := 401929; -- 311
	constant c_NOTE_DS5 : integer := 200965; -- 622
	constant c_NOTE_DS6 : integer := 100402; -- 1245
	constant c_NOTE_DS7 : integer := 50221; -- 2489
	constant c_NOTE_DS8 : integer := 25110; -- 4978
	constant c_NOTE_DS9 : integer := 12555; -- 9956

	constant c_NOTE_E0 : integer := 5952381; -- 21
	constant c_NOTE_E1 : integer := 3048780; -- 41
	constant c_NOTE_E2 : integer := 1524390; -- 82
	constant c_NOTE_E3 : integer := 757576; -- 165
	constant c_NOTE_E4 : integer := 378788; -- 330
	constant c_NOTE_E5 : integer := 189681; -- 659
	constant c_NOTE_E6 : integer := 94769; -- 1319
	constant c_NOTE_E7 : integer := 47402; -- 2637
	constant c_NOTE_E8 : integer := 23701; -- 5274
	constant c_NOTE_E9 : integer := 11851; -- 10548

	constant c_NOTE_F0 : integer := 5681818; -- 22
	constant c_NOTE_F1 : integer := 2840909; -- 44
	constant c_NOTE_F2 : integer := 1436782; -- 87
	constant c_NOTE_F3 : integer := 714286; -- 175
	constant c_NOTE_F4 : integer := 358166; -- 349
	constant c_NOTE_F5 : integer := 179083; -- 698
	constant c_NOTE_F6 : integer := 89477; -- 1397
	constant c_NOTE_F7 : integer := 44739; -- 2794
	constant c_NOTE_F8 : integer := 22369; -- 5588
	constant c_NOTE_F9 : integer := 11186; -- 11175

	constant c_NOTE_FS0 : integer := 5434783; -- 23
	constant c_NOTE_FS1 : integer := 2717391; -- 46
	constant c_NOTE_FS2 : integer := 1344086; -- 93
	constant c_NOTE_FS3 : integer := 675676; -- 185
	constant c_NOTE_FS4 : integer := 337838; -- 370
	constant c_NOTE_FS5 : integer := 168919; -- 740
	constant c_NOTE_FS6 : integer := 84459; -- 1480
	constant c_NOTE_FS7 : integer := 42230; -- 2960
	constant c_NOTE_FS8 : integer := 21115; -- 5920
	constant c_NOTE_FS9 : integer := 10557; -- 11840

	constant c_NOTE_G0 : integer := 5000000; -- 25
	constant c_NOTE_G1 : integer := 2551020; -- 49
	constant c_NOTE_G2 : integer := 1275510; -- 98
	constant c_NOTE_G3 : integer := 637755; -- 196
	constant c_NOTE_G4 : integer := 318878; -- 392
	constant c_NOTE_G5 : integer := 159439; -- 784
	constant c_NOTE_G6 : integer := 79719; -- 1568
	constant c_NOTE_G7 : integer := 39860; -- 3136
	constant c_NOTE_G8 : integer := 19930; -- 6272
	constant c_NOTE_G9 : integer := 9965; -- 12544

	constant c_NOTE_GS0 : integer := 4807692; -- 26
	constant c_NOTE_GS1 : integer := 2403846; -- 52
	constant c_NOTE_GS2 : integer := 1201923; -- 104
	constant c_NOTE_GS3 : integer := 600962; -- 208
	constant c_NOTE_GS4 : integer := 301205; -- 415
	constant c_NOTE_GS5 : integer := 150421; -- 831
	constant c_NOTE_GS6 : integer := 75256; -- 1661
	constant c_NOTE_GS7 : integer := 37628; -- 3322
	constant c_NOTE_GS8 : integer := 18811; -- 6645
	constant c_NOTE_GS9 : integer := 9406; -- 13290

	constant c_NOTE_A0 : integer := 4629630; -- 27
	constant c_NOTE_A1 : integer := 2272727; -- 55
	constant c_NOTE_A2 : integer := 1136364; -- 110
	constant c_NOTE_A3 : integer := 568182; -- 220
	constant c_NOTE_A4 : integer := 284091; -- 440
	constant c_NOTE_A5 : integer := 142045; -- 880
	constant c_NOTE_A6 : integer := 71023; -- 1760
	constant c_NOTE_A7 : integer := 35511; -- 3520
	constant c_NOTE_A8 : integer := 17756; -- 7040
	constant c_NOTE_A9 : integer := 8878; -- 14080

	constant c_NOTE_AS0 : integer := 4310345; -- 29
	constant c_NOTE_AS1 : integer := 2155172; -- 58
	constant c_NOTE_AS2 : integer := 1068376; -- 117
	constant c_NOTE_AS3 : integer := 536481; -- 233
	constant c_NOTE_AS4 : integer := 268240; -- 466
	constant c_NOTE_AS5 : integer := 134120; -- 932
	constant c_NOTE_AS6 : integer := 67024; -- 1865
	constant c_NOTE_AS7 : integer := 33521; -- 3729
	constant c_NOTE_AS8 : integer := 16758; -- 7459
	constant c_NOTE_AS9 : integer := 8380; -- 14917

	constant c_NOTE_B0 : integer := 4032258; -- 31
	constant c_NOTE_B1 : integer := 2016129; -- 62
	constant c_NOTE_B2 : integer := 1016260; -- 123
	constant c_NOTE_B3 : integer := 506073; -- 247
	constant c_NOTE_B4 : integer := 253036; -- 494
	constant c_NOTE_B5 : integer := 126518; -- 988
	constant c_NOTE_B6 : integer := 63259; -- 1976
	constant c_NOTE_B7 : integer := 31638; -- 3951
	constant c_NOTE_B8 : integer := 15819; -- 7902
----------------------------------------------------------------------------------------------------------------

	constant c_Music_Notes : Music_Commands_Mem := (
			0 => 0,   -- NOTE_C1 / 16 //  
			1 => c_NOTE_C1,   -- NOTE_C1 / 16 //  
			2 => c_NOTE_C1,   -- NOTE_C1 / 33 //  
			3 => c_NOTE_C2,   -- NOTE_C2 / 65 //  
			4 => c_NOTE_C3,   -- NOTE_C3 / 131 //  
			5 => c_NOTE_C4,   -- NOTE_C4 / 261 //  
			6 => c_NOTE_C5,   -- NOTE_C5 / 523 //  
			7 => c_NOTE_C6,   -- NOTE_C6 / 1047 //  
			8 => c_NOTE_C7,   -- NOTE_C7 / 2093 //  
			9 => c_NOTE_C8,   -- NOTE_C8 / 4186 //  
			10 => c_NOTE_C9,   -- NOTE_C9 / 8372 //  
			11 => c_NOTE_CS0,   -- NOTE_CS0 / 17 //  
			12 => c_NOTE_CS1,   -- NOTE_CS1 / 35 //  
			13 => c_NOTE_CS2,   -- NOTE_CS2 / 69 //  
			14 => c_NOTE_CS3,   -- NOTE_CS3 / 139 //  
			15 => c_NOTE_CS4,   -- NOTE_CS4 / 277 //  
			16 => c_NOTE_CS5,   -- NOTE_CS5 / 554 //  
			17 => c_NOTE_CS6,   -- NOTE_CS6 / 1109 //  
			18 => c_NOTE_CS7,   -- NOTE_CS7 / 2217 //  
			19 => c_NOTE_CS8,   -- NOTE_CS8 / 4435 //  
			20 => c_NOTE_CS9,   -- NOTE_CS9 / 8870 //  
			21 => c_NOTE_D0,   -- NOTE_D0 / 18 //  
			22 => c_NOTE_D1,   -- NOTE_D1 / 37 //  
			23 => c_NOTE_D2,   -- NOTE_D2 / 73 //  
			24 => c_NOTE_D3,   -- NOTE_D3 / 147 //  
			25 => c_NOTE_D4,   -- NOTE_D4 / 294 //  
			26 => c_NOTE_D5,   -- NOTE_D5 / 587 //  
			27 => c_NOTE_D6,   -- NOTE_D6 / 1175 //  
			28 => c_NOTE_D7,   -- NOTE_D7 / 2349 //  
			29 => c_NOTE_D8,   -- NOTE_D8 / 4699 //  
			30 => c_NOTE_D9,   -- NOTE_D9 / 9397 //  
			31 => c_NOTE_DS0,   -- NOTE_DS0 / 19 //  
			32 => c_NOTE_DS1,   -- NOTE_DS1 / 39 //  
			33 => c_NOTE_DS2,   -- NOTE_DS2 / 78 //  
			34 => c_NOTE_DS3,   -- NOTE_DS3 / 156 //  
			35 => c_NOTE_DS4,   -- NOTE_DS4 / 311 //  
			36 => c_NOTE_DS5,   -- NOTE_DS5 / 622 //  
			37 => c_NOTE_DS6,   -- NOTE_DS6 / 1245 //  
			38 => c_NOTE_DS7,   -- NOTE_DS7 / 2489 //  
			39 => c_NOTE_DS8,   -- NOTE_DS8 / 4978 //  
			40 => c_NOTE_DS9,   -- NOTE_DS9 / 9956 //  
			41 => c_NOTE_E0,   -- NOTE_E0 / 21 //  
			42 => c_NOTE_E1,   -- NOTE_E1 / 41 //  
			43 => c_NOTE_E2,   -- NOTE_E2 / 82 //  
			44 => c_NOTE_E3,   -- NOTE_E3 / 165 //  
			45 => c_NOTE_E4,   -- NOTE_E4 / 330 //  
			46 => c_NOTE_E5,   -- NOTE_E5 / 659 //  
			47 => c_NOTE_E6,   -- NOTE_E6 / 1319 //  
			48 => c_NOTE_E7,   -- NOTE_E7 / 2637 //  
			49 => c_NOTE_E8,   -- NOTE_E8 / 5274 //  
			50 => c_NOTE_E9,   -- NOTE_E9 / 10548 //  
			51 => c_NOTE_F0,   -- NOTE_F0 / 22 //  
			52 => c_NOTE_F1,   -- NOTE_F1 / 44 //  
			53 => c_NOTE_F2,   -- NOTE_F2 / 87 //  
			54 => c_NOTE_F3,   -- NOTE_F3 / 175 //  
			55 => c_NOTE_F4,   -- NOTE_F4 / 349 //  
			56 => c_NOTE_F5,   -- NOTE_F5 / 698 //  
			57 => c_NOTE_F6,   -- NOTE_F6 / 1397 //  
			58 => c_NOTE_F7,   -- NOTE_F7 / 2794 //  
			59 => c_NOTE_F8,   -- NOTE_F8 / 5588 //  
			60 => c_NOTE_F9,   -- NOTE_F9 / 11175 //  
			61 => c_NOTE_FS0,   -- NOTE_FS0 / 23 //  
			62 => c_NOTE_FS1,   -- NOTE_FS1 / 46 //  
			63 => c_NOTE_FS2,   -- NOTE_FS2 / 93 //  
			64 => c_NOTE_FS3,   -- NOTE_FS3 / 185 //  
			65 => c_NOTE_FS4,   -- NOTE_FS4 / 370 //  
			66 => c_NOTE_FS5,   -- NOTE_FS5 / 740 //  
			67 => c_NOTE_FS6,   -- NOTE_FS6 / 1480 //  
			68 => c_NOTE_FS7,   -- NOTE_FS7 / 2960 //  
			69 => c_NOTE_FS8,   -- NOTE_FS8 / 5920 //  
			70 => c_NOTE_FS9,   -- NOTE_FS9 / 11840 //  
			71 => c_NOTE_G0,   -- NOTE_G0 / 25 //  
			72 => c_NOTE_G1,   -- NOTE_G1 / 49 //  
			73 => c_NOTE_G2,   -- NOTE_G2 / 98 //  
			74 => c_NOTE_G3,   -- NOTE_G3 / 196 //  
			75 => c_NOTE_G4,   -- NOTE_G4 / 392 //  
			76 => c_NOTE_G5,   -- NOTE_G5 / 784 //  
			77 => c_NOTE_G6,   -- NOTE_G6 / 1568 //  
			78 => c_NOTE_G7,   -- NOTE_G7 / 3136 //  
			79 => c_NOTE_G8,   -- NOTE_G8 / 6272 //  
			80 => c_NOTE_G9,   -- NOTE_G9 / 12544 //  
			81 => c_NOTE_GS0,   -- NOTE_GS0 / 26 //  
			82 => c_NOTE_GS1,   -- NOTE_GS1 / 52 //  
			83 => c_NOTE_GS2,   -- NOTE_GS2 / 104 //  
			84 => c_NOTE_GS3,   -- NOTE_GS3 / 208 //  
			85 => c_NOTE_GS4,   -- NOTE_GS4 / 415 //  
			86 => c_NOTE_GS5,   -- NOTE_GS5 / 831 //  
			87 => c_NOTE_GS6,   -- NOTE_GS6 / 1661 //  
			88 => c_NOTE_GS7,   -- NOTE_GS7 / 3322 //  
			89 => c_NOTE_GS8,   -- NOTE_GS8 / 6645 //  
			90 => c_NOTE_GS9,   -- NOTE_GS9 / 13290 //  
			91 => c_NOTE_A0,   -- NOTE_A0 / 27 //  
			92 => c_NOTE_A1,   -- NOTE_A1 / 55 //  
			93 => c_NOTE_A2,   -- NOTE_A2 / 110 //  
			94 => c_NOTE_A3,   -- NOTE_A3 / 220 //  
			95 => c_NOTE_A4,   -- NOTE_A4 / 440 //  
			96 => c_NOTE_A5,   -- NOTE_A5 / 880 //  
			97 => c_NOTE_A6,   -- NOTE_A6 / 1760 //  
			98 => c_NOTE_A7,   -- NOTE_A7 / 3520 //  
			99 => c_NOTE_A8,   -- NOTE_A8 / 7040 //  
			100 => c_NOTE_A9,   -- NOTE_A9 / 14080 //  
			101 => c_NOTE_AS0,   -- NOTE_AS0 / 29 //  
			102 => c_NOTE_AS1,   -- NOTE_AS1 / 58 //  
			103 => c_NOTE_AS2,   -- NOTE_AS2 / 117 //  
			104 => c_NOTE_AS3,   -- NOTE_AS3 / 233 //  
			105 => c_NOTE_AS4,   -- NOTE_AS4 / 466 //  
			106 => c_NOTE_AS5,   -- NOTE_AS5 / 932 //  
			107 => c_NOTE_AS6,   -- NOTE_AS6 / 1865 //  
			108 => c_NOTE_AS7,   -- NOTE_AS7 / 3729 //  
			109 => c_NOTE_AS8,   -- NOTE_AS8 / 7459 //  
			110 => c_NOTE_AS9,   -- NOTE_AS9 / 14917 //  
			111 => c_NOTE_B0,   -- NOTE_B0 / 31 //  
			112 => c_NOTE_B1,   -- NOTE_B1 / 62 //  
			113 => c_NOTE_B2,   -- NOTE_B2 / 123 //  
			114 => c_NOTE_B3,   -- NOTE_B3 / 247 //  
			115 => c_NOTE_B4,   -- NOTE_B4 / 494 //  
			116 => c_NOTE_B5,   -- NOTE_B5 / 988 //  
			117 => c_NOTE_B6,   -- NOTE_B6 / 1976 //  
			118 => c_NOTE_B7,   -- NOTE_B7 / 3951 //  
			119 => c_NOTE_B8   -- NOTE_B8 / 7902 //  




				);

	constant c_Delay_0 : integer := 312500; -- 
	constant c_Delay_1 : integer := 15625000; -- 
	constant c_Delay_2 : integer := 31250000; -- 
	constant c_Delay_3 : integer := 46875000; -- 
	constant c_Delay_4 : integer := 62500000; -- 
	constant c_Delay_5 : integer := 93750000; -- 
	constant c_Delay_6 : integer := 125000000; -- 
	constant c_Delay_7 : integer := 250000000; -- 
	
	constant c_Music_Mario_Loose : Music_Block := (
			0 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(115, 8)),
			1 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			2 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(56, 8)),
			3 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & "00000000",
			4 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(56, 8)),
			5 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			6 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(56, 8)),
			7 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			8 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(46, 8)),
			9 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			10 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(26, 8)),
			11 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			12 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(6, 8)),
			13 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			14 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(45, 8)),
			15 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			16 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(74, 8)),
			17 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			18 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(5, 8)),
			19 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			20 => std_logic_vector(to_unsigned(c_Delay_3, 28)) & std_logic_vector(to_unsigned(4, 8)),
			21 => std_logic_vector(to_unsigned(c_Delay_6, 28)) & "00000000",
			22 => std_logic_vector(to_unsigned(c_Delay_5, 28)) & "00000000"


			);

	constant c_Music_Mario_Win : Music_Block := (
			0 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(74, 8)),
			1 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			2 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(5, 8)),
			3 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			4 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(45, 8)),
			5 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			6 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(75, 8)),
			7 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			8 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(6, 8)),
			9 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			10 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(46, 8)),
			11 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			12 => std_logic_vector(to_unsigned(c_Delay_3, 28)) & std_logic_vector(to_unsigned(76, 8)),
			13 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			14 => std_logic_vector(to_unsigned(c_Delay_3, 28)) & std_logic_vector(to_unsigned(46, 8)),
			15 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",

			16 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(84, 8)),
			17 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			18 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(5, 8)),
			19 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			20 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(35, 8)),
			21 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			22 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(85, 8)),
			23 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			24 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(6, 8)),
			25 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			26 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(36, 8)),
			27 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			28 => std_logic_vector(to_unsigned(c_Delay_3, 28)) & std_logic_vector(to_unsigned(86, 8)),
			29 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			30 => std_logic_vector(to_unsigned(c_Delay_3, 28)) & std_logic_vector(to_unsigned(36, 8)),
			31 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",

			32 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(104, 8)),
			33 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			34 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(25, 8)),
			35 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			36 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(75, 8)),
			37 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			38 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(105, 8)),
			39 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			40 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(26, 8)),
			41 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			42 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(56, 8)),
			43 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			44 => std_logic_vector(to_unsigned(c_Delay_3, 28)) & std_logic_vector(to_unsigned(106, 8)),
			45 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",

			46 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(106, 8)),
			47 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			48 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(106, 8)),
			49 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			50 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(106, 8)),
			51 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			52 => std_logic_vector(to_unsigned(c_Delay_5, 28)) & std_logic_vector(to_unsigned(7, 8)),
			53 => std_logic_vector(to_unsigned(c_Delay_6, 28)) & "00000000",
			54 => std_logic_vector(to_unsigned(c_Delay_6, 28)) & "00000000"



			);
	
	constant c_Music_Pirates : Music_Block := (
			0 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(45, 8 )),
			1 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			2 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(75, 8 )),
			3 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			4 => std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(95, 8 )),
			5 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			6 => std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(95, 8 )),
			7 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			8 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(95, 8 )),
			9 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			10 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(115, 8 )),
			11 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			12 => std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(6, 8 )),
			13 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			14 => std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(6, 8 )),
			15 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			16 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(6, 8 )),
			17 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			18 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(26 , 8)),	
			19 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			20 => std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(115, 8 )),	
			21 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			22 => std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(115, 8 )),	
			23 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			24 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(95, 8 )),
			25 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			26 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(75, 8 )),	
			27 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			28 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(75, 8 )),
			29 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			30 => std_logic_vector(to_unsigned(c_Delay_3, 28))    & std_logic_vector(to_unsigned(95, 8 )),
			31 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & "00000000", 
		
			32 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(45, 8)),
			33 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			34 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(75, 8)),
			35 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			36 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(95, 8)),
			37 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			38 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(95, 8)),
			39 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			40 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(95, 8)),
			41 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			42 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(115, 8)),
			43 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			44 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(6, 8)),
			45 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			46 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(6, 8)),
			47 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			48 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(6, 8)),
			49 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			50 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(26, 8)),
			51 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			52 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(115, 8)),
			53 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			54 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(115, 8)),
			55 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			56 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(95, 8)),
			57 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			58 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(75, 8)),
			59 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			60 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(95, 8)),
			61 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & "00000000",--------------------------------PAUSA LUNGA
		
			62 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(45, 8)),
			63 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			64 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(75, 8)),
			65 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			66 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(95, 8)),
			67 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			68 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(95, 8)),
			69 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			70 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(95, 8)),
			71 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			72 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(6, 8)),
			73 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			74 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(26, 8)),
			75 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			76 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(26, 8)),
			77 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			78 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(26, 8)),
			79 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			80 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(46, 8)),
			81 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			82 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(56, 8)),
			83 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			84 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(56, 8)),
			85 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			86 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(46, 8)),
			87 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			88 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(26, 8)),
			89 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			90 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(46, 8)),
			91 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			92 => std_logic_vector(to_unsigned(c_Delay_3, 28)) & std_logic_vector(to_unsigned(95, 8)),
			93 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			94 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(95, 8)),
			95 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			96 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(115, 8)),
			97 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			98 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(6, 8)),
			99 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			100 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(6, 8)),
			101 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			102 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(26, 8)),
			103 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			104 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(46, 8)),
			105 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			106 => std_logic_vector(to_unsigned(c_Delay_3, 28)) & std_logic_vector(to_unsigned(95, 8)),
			107 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			108 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(95, 8)),
			109 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			110 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(6, 8)),
			111 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
		
			112 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(115, 8)),
			113 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			114 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(115, 8)),
			115 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			116 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(5, 8)),
			117 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			118 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(95, 8)),
			119 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			120 => std_logic_vector(to_unsigned(c_Delay_4, 28)) & std_logic_vector(to_unsigned(115, 8)),
			121 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & "00000000",
		
			122 =>  std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(45, 8 )),
			123 =>  std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			124 =>  std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(75, 8 )),
			125 =>  std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			126 =>  std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(95, 8 )),
			127 =>  std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			128 =>  std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(95, 8 )),
			129 =>  std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			130 =>  std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(95, 8 )),
			131 =>  std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			132 =>  std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(115, 8 )),
			133 =>  std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			134 =>  std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(6, 8 )),
			135 =>  std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			136 =>  std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(6, 8 )),
			137 =>  std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			138 =>  std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(6, 8 )),
			139 =>  std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			140 =>  std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(26 , 8)),	
			141 =>  std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			142 =>  std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(115, 8 )),	
			143 =>  std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			144 =>  std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(115, 8 )),	
			145 =>  std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			146 =>  std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(95, 8 )),
			147 =>  std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			148 =>  std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(75, 8 )),	
			149 =>  std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			150 =>  std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(75, 8 )),
			151 =>  std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,
			152 =>  std_logic_vector(to_unsigned(c_Delay_3, 28))    & std_logic_vector(to_unsigned(95, 8 )),
			153 =>  std_logic_vector(to_unsigned(c_Delay_1, 28))    & "00000000", 

			154 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(45, 8)),
			155 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			156 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(75, 8)),
			157 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			158 =>  std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(95, 8)),
			159 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			160 =>  std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(95, 8)),
			161 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			162 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(95, 8)),
			163 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			164 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(115, 8)),
			165 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			166 =>  std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(6, 8)),
			167 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			168 =>  std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(6, 8)),
			169 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			170 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(6, 8)),
			171 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			172 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(26, 8)),
			173 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			174 =>  std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(115, 8)),
			175 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			176 =>  std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(115, 8)),
			177 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			178 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(95, 8)),
			179 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			180 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(75, 8)),
			181 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			182 =>  std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(95, 8)),
			183 =>  std_logic_vector(to_unsigned(c_Delay_2, 28)) & "00000000",--------------------------------PAUSA LUNGA

			184 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(45, 8)),
			185 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			186 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(75, 8)),
			187 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			188 =>  std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(95, 8)),
			189 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			190 =>  std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(95, 8)),
			191 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			192 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(95, 8)),
			193 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			194 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(6, 8)),
			195 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			196 =>  std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(26, 8)),
			197 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			198 =>  std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(26, 8)),
			199 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			200 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(26, 8)),
			201 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			202 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(46, 8)),
			203 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			204 =>  std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(56, 8)),
			205 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			206 =>  std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(56, 8)),
			207 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			208 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(46, 8)),
			209 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			210 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(26, 8)),
			211 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			212 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(46, 8)),
			213 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			214 =>  std_logic_vector(to_unsigned(c_Delay_3, 28)) & std_logic_vector(to_unsigned(95, 8)),
			215 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			216 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(95, 8)),
			217 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			218 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(115, 8)),
			219 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			220 =>  std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(6, 8)),
			221 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			222 =>  std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(6, 8)),
			223 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			224 =>  std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(26, 8)),
			225 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			226 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(46, 8)),
			227 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			228 =>  std_logic_vector(to_unsigned(c_Delay_3, 28)) & std_logic_vector(to_unsigned(95, 8)),
			229 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			230 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(95, 8)),
			231 =>  std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			232 =>  std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(6, 8)),

			233 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(115, 8)),
			234 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			235 => std_logic_vector(to_unsigned(c_Delay_2, 28)) & std_logic_vector(to_unsigned(115, 8)),
			236 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			237 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(95, 8)),
			238 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			239 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & std_logic_vector(to_unsigned(75, 8)),
			240 => std_logic_vector(to_unsigned(c_Delay_0, 28)) & "00000000",
			241 => std_logic_vector(to_unsigned(c_Delay_5, 28)) & std_logic_vector(to_unsigned(95, 8)),
			242 => std_logic_vector(to_unsigned(c_Delay_1, 28)) & "00000000"

			);




end Music_Constants;

package body Music_Constants is
 
end Music_Constants;



--constant c_Music_Block_2 : Music_Block := (
		
--			0 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(45, 8 )),		-- 8
--			1 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,		-- 0

--			2 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(75, 8 )),		-- 8
--			3 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,		-- 0

--			4 => std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(95, 8 )),		-- 8
--			5 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,		-- 0

--			6 => std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(95, 8 )),		-- 8
--			7 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,		-- 0

--			8 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(95, 8 )),		-- 8
--			9 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,		-- 0

--			10 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(115, 8 )),		-- 8
--			11 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,		-- 0

--			12 => std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(6, 8 )),		-- 8
--			13 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,		-- 0

--			14 => std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(6, 8 )),		-- 8
--			15 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,		-- 0

--			16 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(6, 8 )),		-- 8
--			17 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,		-- 0

--			18 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(26 , 8)),		-- 8
--			19 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,		-- 0

--			20 => std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(115, 8 )),		-- 8
--			21 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,		-- 0

--			22 => std_logic_vector(to_unsigned(c_Delay_2, 28))    & std_logic_vector(to_unsigned(115, 8 )),		-- 8
--			23 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,		-- 0

--			24 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(95, 8 )),		-- 8
--			25 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,		-- 0

--			26 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(75, 8 )),		-- 8
--			27 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,		-- 0

--			28 => std_logic_vector(to_unsigned(c_Delay_1, 28))    & std_logic_vector(to_unsigned(75, 8 )),		-- 8
--			29 => std_logic_vector(to_unsigned(c_Delay_0, 28))    & "00000000" ,		-- 0

--			30 => std_logic_vector(to_unsigned(c_Delay_3, 28))    & std_logic_vector(to_unsigned(95, 8 )),		-- 8
--			31 => std_logic_vector(to_unsigned(c_Delay_4, 28))    & "00000000" 		-- 0


--			);

