--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:39:18 05/31/2016
-- Design Name:   
-- Module Name:   D:/Develop/Xlinx/PROJECT/Master_Mark_2/TB.vhd
-- Project Name:  Master_Mark_2
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Master_Module
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB IS
END TB;
 
ARCHITECTURE behavior OF TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Master_Module
    PORT(
         i_Master_Clk : in std_logic;
			i_Master_On : in std_logic;
			i_Master_Buttons	: in std_logic_vector(2 downto 0);
			i_Master_Switch_1	: in std_logic;
			i_Master_Switch_2	: in std_logic;
			i_Master_Switch_3	: in std_logic;
					
			o_Master_Led	: out std_logic_vector(3 downto 0);
			o_Master_R 		: out std_logic_vector(4 downto 0);
			o_Master_G 		: out std_logic_vector(5 downto 0);
			o_Master_B 		: out std_logic_vector(4 downto 0);
			
			o_Master_HSync 	: out std_logic;
			o_Master_VSync 	: out std_logic;
			o_Master_Lcd_Data_1 : out std_logic_vector(7 downto 0);
			o_Master_Lcd_Control_1 : out std_logic_vector(6 downto 4);

			o_Master_Lcd_Data_2 : out std_logic_vector(7 downto 0);
			o_Master_Lcd_Control_2 : out std_logic_vector(6 downto 4)
        );
    END COMPONENT;
    

   --Inputs
   signal i_Master_Clk : std_logic := '0';
   signal i_Master_On : std_logic := '0';
	signal i_Master_Buttons	: std_logic_vector(2 downto 0);
	signal i_Master_Switch_1	: std_logic;
	signal i_Master_Switch_2	: std_logic;
	signal i_Master_Switch_3	: std_logic;


 	--Outputs
	signal o_Vga_Led: std_logic_vector(3 downto 0);
   signal o_Master_Led : std_logic_vector(3 downto 0);
   signal o_Master_R : std_logic_vector(4 downto 0);
   signal o_Master_G : std_logic_vector(5 downto 0);
   signal o_Master_B : std_logic_vector(4 downto 0);
   signal o_Master_HSync : std_logic;
   signal o_Master_VSync : std_logic;	
	signal o_Master_Lcd_Data_1 : std_logic_vector(7 downto 0);
	signal o_Master_Lcd_Control_1 : std_logic_vector(6 downto 4);
	signal o_Master_Lcd_Data_2 : std_logic_vector(7 downto 0);
	signal o_Master_Lcd_Control_2 : std_logic_vector(6 downto 4);

   -- Clock period definitions
   constant i_Master_Clk_period : time := 8 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Master_Module PORT MAP (
          i_Master_Clk => i_Master_Clk,
          i_Master_On => i_Master_On,
          i_Master_Buttons => i_Master_Buttons,
          i_Master_Switch_1 => i_Master_Switch_1,   
          i_Master_Switch_2 => i_Master_Switch_2,
          i_Master_Switch_3 => i_Master_Switch_3,
			 
          o_Master_Led => o_Master_Led,
          o_Master_R => o_Master_R,
          o_Master_G => o_Master_G,
          o_Master_B => o_Master_B,
          o_Master_HSync => o_Master_HSync,
          o_Master_VSync => o_Master_VSync,
			 o_Master_Lcd_Data_1 => o_Master_Lcd_Data_1,
			 o_Master_Lcd_Control_1 => o_Master_Lcd_Control_1,
 			 o_Master_Lcd_Data_2 => o_Master_Lcd_Data_2,
			 o_Master_Lcd_Control_2 => o_Master_Lcd_Control_2
        );

   -- Clock process definitions
   i_Master_Clk_process :process
   begin
		i_Master_Clk <= '0';
		wait for i_Master_Clk_period/2;
		i_Master_Clk <= '1';
		wait for i_Master_Clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      i_Master_On <= '0';
      wait for 400 ns;	
		
      i_Master_On <= '1';
      i_Master_Switch_1 <= '1';
      i_Master_Switch_2 <= '1';
      i_Master_Switch_3 <= '1';
		wait for 100 ns;
		
		
		--i_Master_On <= '0';
      --wait for 100 ns;	
      --i_Master_On <= '1';
		
		--wait for 530000 ns;
		--wait for 1060000 ns;
		--i_Master_Buttons <= "100";
		--wait for 100 ns;
		--i_Master_Buttons <= "000";
		
		--wait for 530000 ns;--524250 ns;	 --reset delay
		--i_Master_Buttons <= "010";
		--wait for 100 ns;
		--i_Master_Buttons <= "000";
		
		--wait for 1000 ns;	
		--i_Master_Buttons <= "010";
		--wait for 100 ns;
		--i_Master_Buttons <= "000";
		
		--wait for 1000 ns;	
		--i_Master_Buttons <= "010";
		--wait for 100 ns;
		--i_Master_Buttons <= "000";
		
		--wait for 530000 ns;	
		--i_Master_Buttons <= "001";
		--wait for 100 ns;
		--i_Master_Buttons <= "000";
		
		--wait for 530000 ns;	
		--i_Master_Buttons <= "001";
		--wait for 100 ns;
		--i_Master_Buttons <= "000";
		 
		wait;
   end process;

END;
