library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.Constants.ALL;


entity PrinterManager is
port(
	i_PrintModule_Clk : in std_logic;
	i_PrintModule_Reset	: in std_logic;

	i_PrintModule_State : in FSM_state_Main;
	i_PrintModule_Turn	: in std_logic_vector (1 downto 0);

	o_Printmodule_Request : out std_logic;
	o_Printmodule_Code_Object	: out integer range 0 to 63;
	o_Printmodule_X_Object		: out integer range 0 to 511;
	o_Printmodule_Y_Object		: out integer range 0 to 127
	);
end PrinterManager;

architecture Behavioral of PrinterManager is

	signal r_Printmodule_Request : std_logic;
	signal r_Printmodule_Code_Object :integer range 0 to 63;
	signal r_Printmodule_X_Object	: integer range 0 to 511;
	signal r_Printmodule_Y_Object	: integer range 0 to 127;

	signal r_DelayOk, r_Delay_Move_Ok, r_DelayStart, r_Delay_Move_Start, r_Flag_0, r_Flag_1, r_Flag_2, r_Flag_3, r_Flag_4 	: std_logic;
	signal r_Delay_Counter, r_Command_Counter, r_Delay_Move_Counter	: integer;


begin

	r_DelayOk <= '1' when (r_Delay_Counter = 12500000) else '0';
	r_Delay_Move_Ok <= '1' when (r_Delay_Move_Counter = 4000000) else '0';

	Delay_proc :process (i_PrintModule_Clk)
	begin
		if i_PrintModule_Reset = '1' then
			r_Delay_Counter <= 0;
		end if;
		if (i_PrintModule_Clk = '1' and i_PrintModule_Clk'event) then
			if r_DelayOk = '1' then
				r_Delay_Counter <= 0;
			elsif r_DelayStart = '1' then
				r_Delay_Counter <= r_Delay_Counter + 1;
			end if;
		end if;
	end process;

	Delay_Move_proc :process (i_PrintModule_Clk)
	begin
		if i_PrintModule_Reset = '1' then
			r_Delay_Move_Counter <= 0;
		end if;
		if (i_PrintModule_Clk = '1' and i_PrintModule_Clk'event) then
			if r_Delay_Move_Ok = '1' then
				r_Delay_Move_Counter <= 0;
			elsif r_Delay_Move_Start = '1' then
				r_Delay_Move_Counter <= r_Delay_Move_Counter + 1;
			end if;
		end if;
	end process;

	process (i_PrintModule_Clk)
	begin
		if (i_PrintModule_Clk = '1' and i_PrintModule_Clk'event) then
			if (i_PrintModule_Reset = '1') then
				r_Printmodule_Request <= '0';
				r_Printmodule_Y_Object <= 0;
				r_Printmodule_X_Object <= 0;
				r_Printmodule_Code_Object <= 0;
				r_DelayStart <= '0';
				r_Delay_Move_Start <= '0';
				r_Flag_0 <= '0';
				r_Flag_1 <= '0';
				r_Flag_2 <= '0';
				r_Flag_3 <= '0';
				r_Flag_4 <= '0';
			else
				if (i_PrintModule_State = Idle) then
					if r_DelayOk = '0' and r_DelayStart = '0' and r_Flag_0 = '0' then
						r_Printmodule_Request <= '1';
						r_Printmodule_Code_Object <= 14;
						r_Printmodule_X_Object <= 15;
						r_Printmodule_Y_Object <= 25;
						r_DelayStart <= '1';
						r_Command_Counter <= 0;
					elsif r_DelayStart = '1' and r_DelayOk = '0' and r_Flag_0 = '0' then
						r_Printmodule_Request <= '0';
					elsif r_DelayOk = '1' and r_Flag_0 = '0' then
						r_Command_Counter <= r_Command_Counter + 1;
						case r_Command_Counter is
						when 0 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 14;
							r_Printmodule_X_Object <= 15;
							r_Printmodule_Y_Object <= 25;
						when 1 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 22;
							r_Printmodule_X_Object <= 35;
							r_Printmodule_Y_Object <= 25;
						when 2 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 10;
							r_Printmodule_X_Object <= 55;
							r_Printmodule_Y_Object <= 25;
						when 3 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 23;
							r_Printmodule_X_Object <= 75;
							r_Printmodule_Y_Object <= 25;
						when 4 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 30;
							r_Printmodule_X_Object <= 95;
							r_Printmodule_Y_Object <= 25;
						when 5 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 14;
							r_Printmodule_X_Object <= 115;
							r_Printmodule_Y_Object <= 25;
						when 6 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 21;
							r_Printmodule_X_Object <= 135;
							r_Printmodule_Y_Object <= 25;
						when 7 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 14;
							r_Printmodule_X_Object <= 155;
							r_Printmodule_Y_Object <= 25;
						when others =>
							r_Printmodule_Request <= '0';
							r_Command_Counter <= 0;
							r_DelayStart <= '0';
							r_Flag_0 <= '1';
						end case;
					end if;					
				elsif (i_PrintModule_State = Game_Start and i_PrintModule_Turn = "10") then --HO SCAMBIATO I TURNI; RIMETTI A POSTO!!!
					if r_DelayOk = '0' and r_DelayStart = '0' and r_Flag_1 = '0' then
						r_Printmodule_Request <= '1';
						r_Printmodule_Code_Object <= 27;
						r_Printmodule_X_Object <= 205;
						r_Printmodule_Y_Object <= 25;
						r_DelayStart <= '1';
						r_Command_Counter <= 0;
					elsif r_DelayStart = '1' and r_DelayOk = '0' and r_Flag_1 = '0' then
						r_Printmodule_Request <= '0';
					elsif r_DelayOk = '1' and r_Flag_1 = '0' then
						r_Command_Counter <= r_Command_Counter + 1;
						case r_Command_Counter is
						when 0 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 24;
							r_Printmodule_X_Object <= 225;
							r_Printmodule_Y_Object <= 25;
						when 1 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 12;
							r_Printmodule_X_Object <= 245;
							r_Printmodule_Y_Object <= 25;
						when 2 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 18;
							r_Printmodule_X_Object <= 265;
							r_Printmodule_Y_Object <= 25;
						when 3 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 24;
							r_Printmodule_X_Object <= 285;
							r_Printmodule_Y_Object <= 25;
						when others =>
							r_Printmodule_Request <= '0';
							r_Command_Counter <= 0;
							r_DelayStart <= '0';
							r_Flag_1 <= '1';
						end case;	
					end if;	
				elsif (i_PrintModule_State = Game_Start and i_PrintModule_Turn = "01") then --HO SCAMBIATO I TURNI; RIMETTI A POSTO!!!
					if r_DelayOk = '0' and r_DelayStart = '0' and r_Flag_2 = '0' then
						r_Printmodule_Request <= '1';
						r_Printmodule_Code_Object <= 15;
						r_Printmodule_X_Object <= 185;
						r_Printmodule_Y_Object <= 25;
						r_DelayStart <= '1';
					elsif r_DelayStart = '1' and r_DelayOk = '0' then
						r_Printmodule_Request <= '0';	
					elsif r_DelayOk = '1' then
						r_DelayStart <= '0';
						r_Flag_2 <= '1';
					end if;
				elsif (i_PrintModule_State = Game_Play and i_PrintModule_Turn = "01") then
					if r_DelayOk = '0' and r_Delay_Move_Start = '0' and r_Flag_3 = '0' then
						r_Printmodule_Request <= '1';
						r_Printmodule_Code_Object <= 46;
						r_Printmodule_X_Object <= 20;
						r_Printmodule_Y_Object <= 70;
						r_Delay_Move_Start <= '1';
					elsif r_Delay_Move_Start = '1' and r_DelayOk = '0' then
						r_Printmodule_Request <= '0';	
					elsif r_DelayOk = '1' then
						r_Delay_Move_Start <= '0';
						r_Flag_3 <= '1';
					end if;
				elsif (i_PrintModule_State = Game_Play and i_PrintModule_Turn = "10") then 
					if r_Delay_Move_Ok = '0' and r_Delay_Move_Start = '0' and r_Flag_4 = '0' then
						r_Printmodule_Request <= '1';
						r_Printmodule_Code_Object <= 46;
						r_Printmodule_X_Object <= 20;
						r_Printmodule_Y_Object <= 70;
						r_Delay_Move_Start <= '1';
						r_Command_Counter <= 0;
					elsif r_Delay_Move_Start = '1' and r_Delay_Move_Ok = '0' and r_Flag_4 = '0' then
						r_Printmodule_Request <= '0';
					elsif r_Delay_Move_Ok = '1' and r_Flag_4 = '0' then
						r_Command_Counter <= r_Command_Counter + 1;
						case r_Command_Counter is
						when 0 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 1 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 2 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 3 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 4 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 5 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 6 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 7 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 8 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 9 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 10 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 11 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 12 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 13 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 14 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 15 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 16 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 17 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 18 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 19 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 20 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 21 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 22 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 23 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 24 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 25 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 26 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 27 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 28 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 29 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 30 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 31 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 32 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 33 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 34 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 35 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 36 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 37 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 38 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 39 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 40 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 41 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 42 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 43 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 44 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 45 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 46 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 47 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 48 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 49 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 50 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 51 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 52 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 53 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 54 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 55 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 56 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 57 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 58 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 59 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 60 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 61 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 62 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 63 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 47;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 64 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 48;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 65 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 49;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when 66 =>
							r_Printmodule_Request <= '1';
							r_Printmodule_Code_Object <= 46;
							r_Printmodule_X_Object <= r_Printmodule_X_Object + 6;
							r_Printmodule_Y_Object <= 70;
						when others =>
							r_Printmodule_Request <= '0';
							r_Command_Counter <= 0;
							r_Delay_Move_Start <= '0';
							r_Flag_4 <= '1';
						end case;	
					end if;
				end if ;
			end if ;
		end if ;
	end process;
	
	o_Printmodule_Request <= r_Printmodule_Request;
	o_Printmodule_Code_Object <= r_Printmodule_Code_Object;
	o_Printmodule_X_Object <= r_Printmodule_X_Object;
	o_Printmodule_Y_Object <= r_Printmodule_Y_Object;

end Behavioral;

