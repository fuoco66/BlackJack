library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.Constants.All;
use work.Lcd_Constants.All;


entity Lcd_Manager is
port(
	i_LcdManager_Clk 		: in std_logic;
	i_LcdManager_FSM_State 	: in FSM_State_Main;

	--i_LcdManager_Switch_1	: in std_logic;
	--i_LcdManager_Selector 	: in std_logic_vector(1 downto 0);

	i_LcdManager_Turn 		: in std_logic_vector(1 downto 0);

	i_LcdManager_P1_Card1	: in std_logic_vector (5 downto 0);
	i_LcdManager_P1_Card2	: in std_logic_vector (5 downto 0);
	i_LcdManager_P2_Card1	: in std_logic_vector (5 downto 0);
	i_LcdManager_P2_Card2	: in std_logic_vector (5 downto 0);

	o_LcdManager_Data_1		: out std_logic_vector(7 downto 0);		--output bus, used for data transfer (DB)
	o_LcdManager_Control_1	: out std_logic_vector (6 downto 4);

	o_LcdManager_Data_2		: out std_logic_vector(7 downto 0);		--output bus, used for data transfer (DB)
	o_LcdManager_Control_2	: out std_logic_vector (6 downto 4)

	);
end Lcd_Manager;

architecture Behavioral of Lcd_Manager is

	Component Lcd_Module is
	port (
		i_Lcd_Clk		: in std_logic;
		i_Lcd_On 		: in std_logic;
		
		--i_Lcd_Command_Code : in Lcd_Commands_Code_Block(0 to 3);
		--i_Lcd_Command_Code : in Lcd_Commands_Code_Block(0 to 4);
		i_Lcd_Command_Code : in Lcd_Commands_Code_Block(0 to 6);
		o_Lcd_BusyFlag 		: out std_logic;
		
		o_Lcd_Data		: out std_logic_vector(7 downto 0);		--output bus, used for data transfer (DB)
		o_Lcd_Control	: out std_logic_vector (6 downto 4)


		);
	end Component Lcd_Module;

	--signal r_LcdManager_Switch_Old_1, r_LcdManager_Switch_Old_2 :std_logic;
	
	signal r_On, r_Lcd_Busy_1, r_Lcd_Busy_2, r_PrintTurn :std_logic;

	--signal r_Lcd_NewCommand_1, r_Lcd_NewCommand_2 : Lcd_Commands_Code_Block(0 to 3); 
	--signal r_Lcd_NewCommand_1, r_Lcd_NewCommand_2 : Lcd_Commands_Code_Block(0 to 4); 
	signal r_Lcd_NewCommand_1, r_Lcd_NewCommand_2 : Lcd_Commands_Code_Block(0 to 6); 

	signal r_Lcd_Selector 	: std_logic_vector(1 downto 0);


	signal r_Delay_Counter_CommandSender : integer range 0 to 256:=0;
	signal r_DelayOk_CommandSender : std_logic;


	--signal r_Send_Command_1 : Lcd_Commands_Code_Block(0 to 3);
	--signal r_Send_Command_2 : Lcd_Commands_Code_Block(0 to 3);

	--signal r_Send_Command_1 : Lcd_Commands_Code_Block(0 to 4);
	--signal r_Send_Command_2 : Lcd_Commands_Code_Block(0 to 4);
	
	signal r_Send_Command_1 : Lcd_Commands_Code_Block(0 to 6);
	signal r_Send_Command_2 : Lcd_Commands_Code_Block(0 to 6);



	signal r_MainFSM_OLD : FSM_State_Main;

	signal r_LcdManager_Selector 	: std_logic_vector(1 downto 0);
	
	signal r_Turn 	: std_logic_vector(1 downto 0);
	
	signal r_P1_Card1	: std_logic_vector (5 downto 0);
	signal r_P1_Card2	: std_logic_vector (5 downto 0);
	signal r_P2_Card1	: std_logic_vector (5 downto 0);
	signal r_P2_Card2	: std_logic_vector (5 downto 0);

begin

	Lcd_1 : Lcd_Module
	port map (
		i_LcdManager_Clk,
		r_On,
		r_Lcd_NewCommand_1,
		r_Lcd_Busy_1,
		o_LcdManager_Data_1,
		o_LcdManager_Control_1
		);
	Lcd_2 : Lcd_Module
	port map (
		i_LcdManager_Clk,
		r_On,
		r_Lcd_NewCommand_2,
		r_Lcd_Busy_2,
		o_LcdManager_Data_2,
		o_LcdManager_Control_2
		);

	Current : process (i_LcdManager_Clk)
	begin
		if i_LcdManager_Clk = '1' and i_LcdManager_Clk'Event then
			if(i_LcdManager_FSM_State /= Reset) then
				r_On <= '1';

				--r_LcdManager_Switch_Old_1 <= i_LcdManager_Switch_1;
				
				r_Turn <= i_LcdManager_Turn;

				r_P1_Card1	<= i_LcdManager_P1_Card1;
				r_P1_Card2	<= i_LcdManager_P1_Card2;
				r_P2_Card1	<= i_LcdManager_P2_Card1;
				r_P2_Card2	<= i_LcdManager_P2_Card2;
				
				if(r_Lcd_Busy_1 = '0' and r_Lcd_Busy_2 = '0') then
					r_MainFSM_OLD <= i_LcdManager_FSM_State;
				
				end if;
				
			else
				r_On <= '0';
				r_MainFSM_OLD <= Reset;
				
				r_Turn 		<= (others => '0');
				r_P1_Card1	<= (others => '0');
				r_P1_Card2	<= (others => '0');
				r_P2_Card1	<= (others => '0');
				r_P2_Card2	<= (others => '0');
				
				--r_LcdManager_Switch_Old_1 <= '0';
				--r_LcdManager_Switch_Old_2 <='0';
			end if;
			
		end if;
	end process;

	r_DelayOk_CommandSender <= '1' when (r_Delay_Counter_CommandSender = 256) else '0'; --255	

	Delay_proc :process (i_LcdManager_Clk, r_DelayOk_CommandSender)
	begin
		if (i_LcdManager_Clk = '1' and i_LcdManager_Clk'event) then
			
			if(i_LcdManager_FSM_State /= Reset) then
			
				if r_DelayOk_CommandSender = '1' then

					r_Delay_Counter_CommandSender <= 0;

					r_Lcd_NewCommand_1(0) <= (others => '0');
					r_Lcd_NewCommand_2(0) <= (others => '0');

				elsif r_Send_Command_1(0) /= "0000000" or r_Send_Command_2(0) /= "0000000" then
					r_Lcd_NewCommand_1 <= r_Send_Command_1;
					r_Lcd_NewCommand_2 <= r_Send_Command_2;

					r_Delay_Counter_CommandSender <= r_Delay_Counter_CommandSender + 1;

				end if;

			else
				r_Lcd_NewCommand_1(0) <= (others => '0');
				r_Lcd_NewCommand_2(0) <= (others => '0');
			end if;
		end if;
	end process;

	Main_Proc : process (i_LcdManager_Clk, i_LcdManager_FSM_State, i_LcdManager_Turn)-- i_LcdManager_On)
	begin
	
		if (i_LcdManager_Clk = '1' and i_LcdManager_Clk'event) then

			if ( (i_LcdManager_FSM_State = New_Game ) and (i_LcdManager_Turn /= r_Turn) and (i_LcdManager_Turn /= "11") and (r_Lcd_Busy_1 = '0') and (r_Lcd_Busy_2 = '0') ) then --i_LcdManager_FSM_State = Game_Start or 

				case( i_LcdManager_Turn ) is				
					when "01" => 
						--Set Turn TO Player #1
						r_Send_Command_1(0) <= (others => '1');
						r_Send_Command_1(1) <= std_logic_vector(to_unsigned( 0, 7 ));--"0111";
						r_Send_Command_1(2) <= std_logic_vector(to_unsigned( 1, 7 ));--"0111";
						r_Send_Command_1(3) <= (others => '0');

						r_Send_Command_2(0) <= (others => '1');
						r_Send_Command_2(1) <= std_logic_vector(to_unsigned( 0, 7 ));--"0111";
						r_Send_Command_2(2) <= (others => '0');
					
					--when "10" =>
					--	--Remove Turn To Player #1
					--	--r_Send_Command_1(0) <= "11111";
					--	r_Send_Command_1(1) <= std_logic_vector(to_unsigned( 2, 7 ));--"1000";
					--	r_Send_Command_1(2) <= (others => '0');

					--	--Set Turn TO Player #2
					--	r_Send_Command_2(0) <= "11111";
					--	r_Send_Command_2(1) <= std_logic_vector(to_unsigned( 1, 7 ));--"0111";
					--	r_Send_Command_2(2) <= (others => '0');
					
					--when "00" =>
					--	r_Send_Command_1(0) <= "11111";
					--	r_Send_Command_1(1) <= std_logic_vector(to_unsigned( 2, 7 ));--"1000";
					--	r_Send_Command_1(2) <= (others => '0');

					--	r_Send_Command_2(0) <= "11111";
					--	r_Send_Command_2(1) <= std_logic_vector(to_unsigned( 2, 7 ));--"1000";
					--	r_Send_Command_2(2) <= (others => '0');
					when others =>

				end case ;
				
			elsif ( ( i_LcdManager_FSM_State = Game_Play ) and (i_LcdManager_Turn /= r_Turn) and (i_LcdManager_Turn /= "11") and (r_Lcd_Busy_1 = '0') and (r_Lcd_Busy_2 = '0') ) then --i_LcdManager_FSM_State = Game_Start or 

				case( i_LcdManager_Turn ) is				
					when "01" => 
						--Set Turn TO Player #1
						r_Send_Command_1(0) <= (others => '1');
						r_Send_Command_1(1) <= std_logic_vector(to_unsigned( 1, 7 ));--"0111";
						r_Send_Command_1(2) <= (others => '0');

						--Remove Turn To Player #2
						r_Send_Command_2(0) <= (others => '0');
						r_Send_Command_2(1) <= std_logic_vector(to_unsigned( 2, 7 ));--"1000";
						r_Send_Command_2(2) <= (others => '0');
					
					when "10" =>
						--Remove Turn To Player #1
						r_Send_Command_1(0) <= (others => '1');
						r_Send_Command_1(1) <= std_logic_vector(to_unsigned( 2, 7 ));--"1000";
						r_Send_Command_1(2) <= (others => '0');

						--Set Turn TO Player #2
						r_Send_Command_2(0) <= (others => '1');
						r_Send_Command_2(1) <= std_logic_vector(to_unsigned( 1, 7 ));--"0111";
						r_Send_Command_2(2) <= (others => '0');
					
					when "00" =>
						r_Send_Command_1(0) <= (others => '1');
						r_Send_Command_1(1) <= std_logic_vector(to_unsigned( 2, 7 ));--"1000";
						r_Send_Command_1(2) <= (others => '0');

						r_Send_Command_2(0) <= (others => '1');
						r_Send_Command_2(1) <= std_logic_vector(to_unsigned( 2, 7 ));--"1000";
						r_Send_Command_2(2) <= (others => '0');
					when others =>

				end case ;


			elsif ( (i_LcdManager_FSM_State = Game_Start) and ((i_LcdManager_Turn /= r_Turn) or (i_LcdManager_P1_Card1 /= r_P1_Card1) or (i_LcdManager_P1_Card2 /= r_P1_Card2) or  (i_LcdManager_P2_Card1 /= r_P2_Card1) or (i_LcdManager_P2_Card2 /= r_P2_Card2) ) and (r_Lcd_Busy_1 = '0') and (r_Lcd_Busy_2 = '0') ) then

				case( i_LcdManager_Turn ) is				
					when "01" =>
						if( (i_LcdManager_P1_Card1 /= r_P1_Card1) )then
							--Set card #1 to Player #1							
							r_Send_Command_1(0) <= (others => '1');
							r_Send_Command_1(1) <= std_logic_vector(to_unsigned( 3, 7 ));--"1001";

							case( i_LcdManager_P1_Card1(3 downto 0) ) is
								when "0001" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Asso, 7 ));--"1001";
								when "0010" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Due, 7 ));--"1001";
								when "0011" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Tre, 7 ));--"1001";
								when "0100" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Quattro, 7 ));--"1001";
								when "0101" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Cinque, 7 ));--"1001";
								when "0110" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Sei, 7 ));--"1001";
								when "0111" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Sette, 7 ));--"1001";
								when "1000" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Otto, 7 ));--"1001";
								when "1001" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Nove, 7 ));--"1001";
								when "1010" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Dieci, 7 ));--"1001";
								when "1011" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Jack, 7 ));--"1001";
								when "1100" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Queen, 7 ));--"1001";
								when "1101" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_King, 7 ));--"1001";
								when others =>
							
							end case ;

							--r_Send_Command_1(3) <= std_logic_vector(to_unsigned( 7, 7 ));

							case( i_LcdManager_P1_Card1(5 downto 4) ) is
							
								when "00" =>
									r_Send_Command_1(3) <= std_logic_vector(to_unsigned( c_Lcd_Index_Cuori_1, 7 ));
								when "01" =>
									r_Send_Command_1(3) <= std_logic_vector(to_unsigned( c_Lcd_Index_Quadri_1, 7 ));
								when "10" =>
									r_Send_Command_1(3) <= std_logic_vector(to_unsigned( c_Lcd_Index_Fiori_1, 7 ));
								when "11" =>
									r_Send_Command_1(3) <= std_logic_vector(to_unsigned( c_Lcd_Index_Picche_1, 7 ));
							
								when others =>
							
							end case ;

							r_Send_Command_1(4) <= (others => '0');


						elsif ( (i_LcdManager_P1_Card2 /= r_P1_Card2) ) then
							--Set card #2 to Player #1 && Remove Turn To Player #1
							r_Send_Command_1(0) <= (others => '1');
							r_Send_Command_1(1) <= std_logic_vector(to_unsigned( 4, 7 ));--"1001";

							case( i_LcdManager_P1_Card2(3 downto 0) ) is
								when "0001" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Asso, 7 ));--"1001";
								when "0010" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Due, 7 ));--"1001";
								when "0011" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Tre, 7 ));--"1001";
								when "0100" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Quattro, 7 ));--"1001";
								when "0101" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Cinque, 7 ));--"1001";
								when "0110" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Sei, 7 ));--"1001";
								when "0111" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Sette, 7 ));--"1001";
								when "1000" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Otto, 7 ));--"1001";
								when "1001" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Nove, 7 ));--"1001";
								when "1010" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Dieci, 7 ));--"1001";
								when "1011" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Jack, 7 ));--"1001";
								when "1100" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Queen, 7 ));--"1001";
								when "1101" =>
									r_Send_Command_1(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_King, 7 ));--"1001";
								when others =>
							
							end case ;

							--r_Send_Command_1(3) <= std_logic_vector(to_unsigned( 7, 7 ));

							case( i_LcdManager_P1_Card2(5 downto 4) ) is
							
								when "00" =>
									r_Send_Command_1(3) <= std_logic_vector(to_unsigned( c_Lcd_Index_Cuori_2, 7 ));
								when "01" =>
									r_Send_Command_1(3) <= std_logic_vector(to_unsigned( c_Lcd_Index_Quadri_2, 7 ));
								when "10" =>
									r_Send_Command_1(3) <= std_logic_vector(to_unsigned( c_Lcd_Index_Fiori_2, 7 ));
								when "11" =>
									r_Send_Command_1(3) <= std_logic_vector(to_unsigned( c_Lcd_Index_Picche_2, 7 ));
							
								when others =>
							
							end case ;


							r_Send_Command_1(4) <= std_logic_vector(to_unsigned( 2, 7 ));--"1000";
							--r_Send_Command_1(3) <= (others => '0');

							--Set Turn TO Player #2
							r_Send_Command_2(0) <= (others => '1');
							r_Send_Command_2(1) <= std_logic_vector(to_unsigned( 1, 7 ));--"0111";
							r_Send_Command_2(2) <= (others => '0');
						end if;
					
					when "10" =>
						if( (i_LcdManager_P2_Card1 /= r_P2_Card1) )then
							--Set card #1 to Player #2
							--r_Send_Command_2(0) <= (others => '1');
							--r_Send_Command_2(1) <= std_logic_vector(to_unsigned( 3, 7 ));--"1001";
							--r_Send_Command_2(2) <= (others => '0');

							r_Send_Command_2(0) <= (others => '1');
							r_Send_Command_2(1) <= std_logic_vector(to_unsigned( 3, 7 ));--"1001";

							case( i_LcdManager_P2_Card1(3 downto 0) ) is
								when "0001" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Asso, 7 ));--"1001";
								when "0010" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Due, 7 ));--"1001";
								when "0011" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Tre, 7 ));--"1001";
								when "0100" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Quattro, 7 ));--"1001";
								when "0101" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Cinque, 7 ));--"1001";
								when "0110" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Sei, 7 ));--"1001";
								when "0111" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Sette, 7 ));--"1001";
								when "1000" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Otto, 7 ));--"1001";
								when "1001" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Nove, 7 ));--"1001";
								when "1010" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Dieci, 7 ));--"1001";
								when "1011" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Jack, 7 ));--"1001";
								when "1100" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Queen, 7 ));--"1001";
								when "1101" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_King, 7 ));--"1001";
								when others =>
							
							end case ;

							--r_Send_Command_1(3) <= std_logic_vector(to_unsigned( 7, 7 ));

							case( i_LcdManager_P2_Card1(5 downto 4) ) is
							
								when "00" =>
									r_Send_Command_2(3) <= std_logic_vector(to_unsigned( c_Lcd_Index_Cuori_1, 7 ));
								when "01" =>
									r_Send_Command_2(3) <= std_logic_vector(to_unsigned( c_Lcd_Index_Quadri_1, 7 ));
								when "10" =>
									r_Send_Command_2(3) <= std_logic_vector(to_unsigned( c_Lcd_Index_Fiori_1, 7 ));
								when "11" =>
									r_Send_Command_2(3) <= std_logic_vector(to_unsigned( c_Lcd_Index_Picche_1, 7 ));
							
								when others =>
							
							end case ;

							r_Send_Command_2(4) <= (others => '0');
						
						elsif ( (i_LcdManager_P2_Card2 /= r_P2_Card2) ) then
							--Set card #2 to Player #2 && Remove Turn To Player #2
							--r_Send_Command_2(0) <= (others => '1');
							--r_Send_Command_2(1) <= std_logic_vector(to_unsigned( 4, 7 ));--"1010";
							--r_Send_Command_2(2) <= std_logic_vector(to_unsigned( 2, 7 ));--"1000";
							--r_Send_Command_2(3) <= (others => '0');

							r_Send_Command_2(0) <= (others => '1');
							r_Send_Command_2(1) <= std_logic_vector(to_unsigned( 4, 7 ));--"1001";

							case( i_LcdManager_P2_Card2(3 downto 0) ) is
								when "0001" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Asso, 7 ));--"1001";
								when "0010" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Due, 7 ));--"1001";
								when "0011" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Tre, 7 ));--"1001";
								when "0100" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Quattro, 7 ));--"1001";
								when "0101" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Cinque, 7 ));--"1001";
								when "0110" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Sei, 7 ));--"1001";
								when "0111" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Sette, 7 ));--"1001";
								when "1000" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Otto, 7 ));--"1001";
								when "1001" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Nove, 7 ));--"1001";
								when "1010" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Dieci, 7 ));--"1001";
								when "1011" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Jack, 7 ));--"1001";
								when "1100" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_Queen, 7 ));--"1001";
								when "1101" =>
									r_Send_Command_2(2) <= std_logic_vector(to_unsigned( c_Lcd_Index_King, 7 ));--"1001";
								when others =>
							
							end case ;

							--r_Send_Command_1(3) <= std_logic_vector(to_unsigned( 7, 7 ));

							case( i_LcdManager_P2_Card2(5 downto 4) ) is
							
								when "00" =>
									r_Send_Command_2(3) <= std_logic_vector(to_unsigned( c_Lcd_Index_Cuori_2, 7 ));
								when "01" =>
									r_Send_Command_2(3) <= std_logic_vector(to_unsigned( c_Lcd_Index_Quadri_2, 7 ));
								when "10" =>
									r_Send_Command_2(3) <= std_logic_vector(to_unsigned( c_Lcd_Index_Fiori_2, 7 ));
								when "11" =>
									r_Send_Command_2(3) <= std_logic_vector(to_unsigned( c_Lcd_Index_Picche_2, 7 ));
							
								when others =>
							
							end case ;

							r_Send_Command_2(4) <= std_logic_vector(to_unsigned( 2, 7 ));--"1000";

						end if;

					when others =>

				end case ;
				
			elsif ( (r_MainFSM_OLD /= i_LcdManager_FSM_State) and ( i_LcdManager_FSM_State = Idle ) and (r_Lcd_Busy_1 = '0') and (r_Lcd_Busy_2 = '0') ) then --i_LcdManager_FSM_State = Game_Start or 
				r_Send_Command_1(0) <= (others => '1');
				r_Send_Command_1(1) <= std_logic_vector(to_unsigned( 0, 7 ));--"0111";
				r_Send_Command_1(2) <= std_logic_vector(to_unsigned( 29, 7 ));--"0111";
				r_Send_Command_1(3) <= std_logic_vector(to_unsigned( 30, 7 ));--"0111";
				r_Send_Command_1(4) <= (others => '0');

				r_Send_Command_2(0) <= (others => '1');
				r_Send_Command_2(1) <= std_logic_vector(to_unsigned( 0, 7 ));--"0111";
				r_Send_Command_2(2) <= std_logic_vector(to_unsigned( 29, 7 ));--"0111";
				r_Send_Command_2(3) <= std_logic_vector(to_unsigned( 30, 7 ));--"0111";
				r_Send_Command_2(4) <= (others => '0');

			elsif ( (r_MainFSM_OLD /= i_LcdManager_FSM_State) and ( i_LcdManager_FSM_State = Main_Init ) and (r_Lcd_Busy_1 = '0') and (r_Lcd_Busy_2 = '0') ) then --i_LcdManager_FSM_State = Game_Start or 
				--r_Send_Command_1(0) <= (others => '1');
				--r_Send_Command_1(1) <= std_logic_vector(to_unsigned( 0, 7 ));
				--r_Send_Command_1(2) <= std_logic_vector(to_unsigned( 5, 7 ));

				----r_Send_Command_1(3) <= std_logic_vector(to_unsigned( 6, 7 ));
				--r_Send_Command_1(3) <= (others => '0');

				--r_Send_Command_2(0) <= (others => '1');
				--r_Send_Command_2(1) <= std_logic_vector(to_unsigned( 0, 7 ));
				--r_Send_Command_2(2) <= std_logic_vector(to_unsigned( 5, 7 ));
				
				----r_Send_Command_2(3) <= std_logic_vector(to_unsigned( 6, 7 ));
				--r_Send_Command_2(3) <= (others => '0');


			elsif r_Delay_Counter_CommandSender = 256 then
				r_Send_Command_1(0) <= (others => '0');
				r_Send_Command_2(0) <= (others => '0');

			else
				--r_Send_Command_1 <= r_Send_Command_1;
				--r_Send_Command_2 <= r_Send_Command_2;

			end if;


		end if;
	

	end process;

end Behavioral;




--FUNZIONANTE PER I TURNI

--if ( ( i_LcdManager_FSM_State = Game_Play or i_LcdManager_FSM_State = Game_Start or i_LcdManager_FSM_State = New_Game ) and (i_LcdManager_Turn /= r_Turn) and (r_Lcd_Busy_1 = '0') and (r_Lcd_Busy_2 = '0') ) then --i_LcdManager_FSM_State = Game_Start or 

--				case( i_LcdManager_Turn ) is				
--					when "01" =>
--						--r_LcdManager_Selector <= "01";

--						r_Send_Command_1(0) <= "1111";
--						r_Send_Command_1(1) <= "0111";
--						r_Send_Command_1(2) <= (others => '0');

--						r_Send_Command_2(0) <= "0000";
--						--r_Send_Command_2(0) <= "1111";
--						r_Send_Command_2(1) <= "1000";
--						r_Send_Command_2(2) <= (others => '0');
					
--					when "10" =>
--						--r_LcdManager_Selector <= "11";

--						r_Send_Command_1(0) <= "1111";
--						r_Send_Command_1(1) <= "1000";
--						r_Send_Command_1(2) <= (others => '0');

--						r_Send_Command_2(0) <= "1111";
--						r_Send_Command_2(1) <= "0111";
--						r_Send_Command_2(2) <= (others => '0');
					
--					when others =>
--						--r_LcdManager_Selector <= "11";

--						r_Send_Command_1(0) <= "1111";
--						r_Send_Command_1(1) <= "1000";
--						r_Send_Command_1(2) <= (others => '0');

--						r_Send_Command_2(0) <= "1111";
--						r_Send_Command_2(1) <= "1000";
--						r_Send_Command_2(2) <= (others => '0');
--				end case ;
--			
