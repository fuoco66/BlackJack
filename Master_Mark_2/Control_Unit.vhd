library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Constants.ALL;

entity Control_Unit is
port (
	i_CU_Clk		: in std_logic;
	i_CU_On 		: in std_logic;
	--i_CU_EndGame	: in std_logic;
	i_CU_Buttons 	: in std_logic_vector(2 downto 0);

	i_CU_P1_Buttons	: in std_logic_vector (1 downto 0);
	i_CU_P2_Buttons	: in std_logic_vector (1 downto 0);

	o_CU_Reset		: out std_logic;
	o_CU_FSM_State 	: out FSM_State_Main;
	o_CU_EndGame	: out std_logic;
	o_CU_Turn		: out std_logic_vector (1 downto 0);
	o_CU_Buttons	: out std_logic_vector (2 downto 0);
	o_CU_Player_Buttons	: out std_logic_vector (1 downto 0)
	--o_CU_P2_Buttons	: out std_logic_vector (1 downto 0);
	--o_CU_Led		: out std_logic_vector (3 downto 0);
	--o_Turn_Led: out std_logic_vector(3 downto 0)

	);

end Control_Unit;

architecture Behavioral of Control_Unit is

	Component Input_Manager is
	port (
		i_IM_Clk_125 	: in std_logic;
		i_IM_Buttons	: in std_logic_vector(2 downto 0);
		
		i_IM_P1_Buttons	: in std_logic_vector (1 downto 0);
		i_IM_P2_Buttons	: in std_logic_vector (1 downto 0);

		o_IM_Buttons	: out std_logic_vector(2 downto 0);
		o_IM_P1_Buttons	: out std_logic_vector (1 downto 0);
		o_IM_P2_Buttons	: out std_logic_vector (1 downto 0)
		);
	end Component Input_Manager;

	signal Current_State, Next_State : FSM_State_Main;

	signal r_Reset, r_DelayOK, r_Switch: std_logic;
	signal r_Hit_Counter : integer range 0 to 2;
	signal r_Leds					: std_logic_vector (3 downto 0);
  	signal r_Buttons_Stable : std_logic_vector (2 downto 0);
  	signal r_Button_NewGame_OLD, r_Button_Game_OLD, r_Button_Hit_OLD : std_logic; 
	signal r_Delay_Counter, r_Delay_Limit	: integer :=0;
	signal r_Turn, r_Turn_GF		: integer range 0 to 4:=0;
  	
  	--new turn stuff
	signal r_P1_Buttons_Stable, r_P2_Buttons_Stable : std_logic_vector (1 downto 0);
  	signal r_P1_Buttons_Stable_OLD, r_P2_Buttons_Stable_OLD : std_logic_vector (1 downto 0);

  	signal r_Turn_Binary, r_Hit_Counter_Binary : std_logic_vector(1 downto 0);

	signal r_Hit, r_Stand, r_New_Game, r_Start_DelayCounter, r_Turn_Delay_Ok, r_Game_Start_End, r_Game_Play_End : std_logic;
	signal r_Delay_Counter_Turn	: integer :=0;
  	--end
begin
	
	IM : Input_Manager port map (i_CU_Clk, i_CU_Buttons, i_CU_P1_Buttons, i_CU_P2_Buttons, r_Buttons_Stable, r_P1_Buttons_Stable, r_P2_Buttons_Stable);
	
	r_Turn_Delay_Ok <= '1' when (r_Delay_Counter_Turn = 250000000) else '0'; 	-----------------ritardo per il cambio turno --625000000 -- 5s

	Delay_proc :process (i_CU_Clk)
	begin
		if (i_CU_Clk = '1' and i_CU_Clk'event) then
			if(Current_State = Game_Start or Current_State = Game_Play) then
				if r_Turn_Delay_Ok = '1' then
					r_Delay_Counter_Turn <= 0;
				elsif r_Start_DelayCounter /= '0' then
					r_Delay_Counter_Turn <= r_Delay_Counter_Turn + 1;
				end if;
			end if;
		end if;
	end process;

	Turn :process (i_CU_Clk)
	begin
		if (i_CU_Clk = '1' and i_CU_Clk'event) then
			if (i_CU_On = '0') then
				r_Turn_Binary <= "11";
				r_Hit_Counter_Binary <= "00";
				r_Start_DelayCounter <= '0';
			else
				if(Current_State = Game_Start) then
					if(r_Hit = '1') then
						if(r_Hit_Counter_Binary = "00" or r_Hit_Counter_Binary = "01") then
							if( r_Hit_Counter_Binary = "00" ) then
								r_Hit_Counter_Binary <= "01";
							else
								r_Hit_Counter_Binary <= "10";
							end if;
						end if;
					elsif (r_Hit_Counter_Binary = "10") then

						if(r_Turn_Binary = "01" and r_Turn_Delay_Ok = '1') then
							r_Turn_Binary <= "10";
							r_Hit_Counter_Binary <= "00";
							r_Start_DelayCounter <= '0';

						elsif (r_Turn_Binary = "10"  and r_Turn_Delay_Ok = '1') then
							r_Turn_Binary <= "00";
							r_Hit_Counter_Binary <= "00";
							r_Start_DelayCounter <= '0';
						elsif (r_Turn_Binary = "00"  and r_Turn_Delay_Ok = '1') then --uscita dal Game_Start
							r_Game_Start_End <= '1';
							r_Turn_Binary <= "01";
							r_Hit_Counter_Binary <= "00";
							r_Start_DelayCounter <= '0';
						else
							r_Start_DelayCounter <= '1';

						end if;
						
					end if;

				elsif Current_State = Game_Play then
					if(r_Hit = '1') then
					
					elsif(r_Stand = '1') then
					
						if(r_Turn_Binary = "01") then
							r_Turn_Binary <= "10";
						elsif r_Turn_Binary = "10" then
							r_Turn_Binary <= "00";
							r_Start_DelayCounter <= '1';
						elsif (r_Turn_Binary = "00"  and r_Turn_Delay_Ok = '1') then --uscita dal Game_Start
							r_Start_DelayCounter <= '0';
							r_Turn_Binary <= "11";
						else
							r_Start_DelayCounter <= '1';
						end if;
					elsif r_Turn_Binary = "11" then
						r_Game_Play_End <= '1';
						r_Start_DelayCounter <= '0';
					
					end if;

				elsif (Current_State = New_Game) then
					r_Turn_Binary <= "01";
					r_Start_DelayCounter <= '0';
				elsif (Current_State = Idle) then
					r_Game_Play_End <= '0';
					r_Start_DelayCounter <= '0';
					r_Game_Start_End <= '0';
					r_Turn_Binary <= "11" ;
				end if;
				
			end if;
		end if;
	end process;



	Delay : process (i_CU_Clk, i_CU_On)
	begin
		if (i_CU_Clk = '1' and i_CU_Clk'event) then
			
			if i_CU_On = '0' then
				r_DelayOK <= '0';
				r_Delay_Counter <= 0;

			elsif  (r_Delay_Counter = r_Delay_Limit-2 or r_Delay_Counter = r_Delay_Limit-1) and Current_State /= Game_Finish then
				r_DelayOK <= '1';
				r_Delay_Counter <= r_Delay_Counter + 1;

			elsif  r_Delay_Counter = r_Delay_Limit-1 and Current_State = Game_Finish then
				r_DelayOK <= '1';
				r_Delay_Counter <= r_Delay_Counter + 1;

			elsif  r_Delay_Counter >= r_Delay_Limit then
				r_DelayOK <= '0';
				r_Delay_Counter <= 0;

			elsif (Current_State = Reset_Delay) or (Current_State = New_Game) or (Current_State = Main_Init) or (Current_State = Game_Finish) or (Current_State = End_Game) then
				r_DelayOK <= '0';
				r_Delay_Counter <= r_Delay_Counter + 1;
			end if;

		end if;
	end process;

	Current : process (i_CU_Clk)
	begin
		if i_CU_Clk = '1' and i_CU_Clk'Event then
			if i_CU_On = '1' then 
				Current_State <= Next_State;
			else
				Current_State <= Reset;
			end if;
		end if;
	end process;

	nextP : process (i_CU_Clk)
	begin
		
		if (i_CU_Clk = '1' and i_CU_Clk'event) then
			if i_CU_On = '1' then 
				r_Button_Game_OLD <= r_Buttons_Stable(1);
				r_Button_NewGame_OLD <= r_Buttons_Stable(2);
				r_Button_Hit_OLD <= r_Buttons_Stable(0);
				
				r_P1_Buttons_Stable_OLD(0) <= r_P1_Buttons_Stable(0);
				r_P1_Buttons_Stable_OLD(1) <= r_P1_Buttons_Stable(1);
				r_P2_Buttons_Stable_OLD(0) <= r_P2_Buttons_Stable(0);
				r_P2_Buttons_Stable_OLD(1) <= r_P2_Buttons_Stable(1);
				
				case Current_State is
					when Reset =>
						if i_CU_On = '1' then
							Next_State <= Reset_Delay;
							r_Delay_Limit <= c_Reset_Limit;
						else
							Next_State <= Reset;
							r_Button_NewGame_OLD <= '0'; -- Bruttino
							r_Button_Game_OLD <= '0';
							r_Turn <= 0;
						end if;
					
					when Reset_Delay =>
						if r_DelayOK = '1' then
							Next_State <= Main_Init;
							r_Delay_Limit <= c_Main_Init_Limit;
						else
							Next_State <= Reset_Delay;
						end if;

					when Main_Init =>
						if r_DelayOK = '1' then
							Next_State <= Idle;
							r_Delay_Limit <= c_Idle_Limit;
						else
							Next_State <= Main_Init;
						end if;

					when New_Game =>
						if r_DelayOK = '1' then
							r_Hit_Counter <= 1;
							Next_State <= Game_Start;
						else
							Next_State <= New_Game;
						end if;

					when Game_Start => --primo turno, quello delle 2 carte coperte
						if i_CU_On = '1' then
							
							if r_P1_Buttons_Stable(0) = '1' and r_P1_Buttons_Stable_OLD(0) /= '1' and r_Turn_Binary = "01" and r_Start_DelayCounter = '0' then
								r_Hit <= '1';
								Next_State <= Game_Start;							
							elsif r_P2_Buttons_Stable(0) = '1' and r_P2_Buttons_Stable_OLD(0) /= '1' and r_Turn_Binary = "10" and r_Start_DelayCounter = '0' then
								r_Hit <= '1';
								Next_State <= Game_Start;
							elsif( r_Turn_Binary = "00") then
								if (r_Hit_Counter_Binary = "10") then
									r_Hit <= '0';
								else
									r_Hit <= '1';
								end if;
								Next_State <= Game_Start;
							elsif (r_Game_Start_End = '1') then
								r_Hit <= '0';
								r_Stand <= '0';
								Next_State <= Game_Play;
							else
								r_Hit <= '0';
								r_Stand <= '0';
								Next_State <= Game_Start;
							end if;

						else
							Next_State <= Reset;
						end if;
					
					when Game_Play => --secondo turno
						if i_CU_On = '1' then
							
							if r_P1_Buttons_Stable(0) = '1' and r_P1_Buttons_Stable_OLD(0) /= '1' and r_Turn_Binary = "01" and r_Start_DelayCounter = '0' then
								r_Hit <= '1';
								Next_State <= Game_Play;							
							elsif r_P1_Buttons_Stable(1) = '1' and r_P1_Buttons_Stable_OLD(1) /= '1' and r_Turn_Binary = "01" and r_Start_DelayCounter = '0' then
								r_Stand <= '1';
								Next_State <= Game_Play;							

							elsif r_P2_Buttons_Stable(0) = '1' and r_P2_Buttons_Stable_OLD(0) /= '1' and r_Turn_Binary = "10" and r_Start_DelayCounter = '0' then
								r_Hit <= '1';
								Next_State <= Game_Play;
							elsif r_P2_Buttons_Stable(1) = '1' and r_P2_Buttons_Stable_OLD(1) /= '1' and r_Turn_Binary = "10" and r_Start_DelayCounter = '0' then
								r_Stand <= '1';
								Next_State <= Game_Play;							
							
							elsif( r_Turn_Binary = "00") then
								r_Stand <= '1';
								Next_State <= Game_Play;								
							elsif r_Turn_Binary = "11" then
								r_Turn <= 0;
								Next_State <= Game_Finish;
								r_Delay_Limit <= c_Game_Finish_Limit;

							else
								r_Hit <= '0';
								r_Stand <= '0';
								Next_State <= Game_Play;
							end if;
							
						else
							Next_State <= Reset;
						end if;


					when Game_Finish => --qui si scoprono le carte!!!!
						if i_CU_On = '1' then
														
							if(r_Turn_GF < 4) and Next_State = Game_Finish and r_DelayOK = '1' then
									r_Turn_GF <= r_Turn_GF +1;
									Next_State <= Game_Finish;

							elsif r_Turn_GF = 4 then
								r_Turn_GF <= 0;
								Next_State <= End_Game;
								r_Delay_Limit <= c_End_Game_Limit;

							elsif r_DelayOK = '0' and Next_State = Game_Finish then
								Next_State <= Game_Finish;
							end if;
						
						else
							Next_State <= Reset;
						end if;

					---------------------------------------------------------------------

					when End_Game => --stato finale, 2s, poi va in idle
						if r_DelayOK = '1' then
							Next_State <= Idle;
							r_Delay_Limit <= c_Idle_Limit;
						else
							Next_State <= End_Game;
						end if;

						
					when Idle =>	--punto di partenza!!!!!!
						if i_CU_On = '1' then

							if(r_Buttons_Stable(2) = '1' and r_Button_NewGame_OLD /= '1') then -- tasto stabile per new game
								Next_State <= New_Game;
								r_Delay_Limit <= c_New_Game_Limit;

							elsif Next_State = Idle then
								Next_State <= Idle;
							end if;

						else
							Next_State <= Reset;
						end if;
				end case;
			
			else
				Next_State <= Reset;
				r_Button_Game_OLD <= '0';
				r_Button_NewGame_OLD <= '0';
				r_Button_Hit_OLD <= '0';

				r_P1_Buttons_Stable_OLD <= "00";
				r_P2_Buttons_Stable_OLD <= "00";

				r_Hit <= '0';
				r_Stand <= '0';
				r_New_Game <= '0';
			end if;
		end if;
	end process;

	output : process (i_CU_Clk)
	begin 
		if (i_CU_Clk = '1' and i_CU_Clk'event) then

				case Current_State is
					when Reset =>
						r_Reset <= '1';
						r_Leds <= "0001";
						--o_Turn_Led 	<= not "0000";

						o_CU_Turn <= "11"; --TO CHECK

					when Reset_Delay =>
						r_Reset <= '1';
						r_Leds <= "0010";

						--o_Turn_Led 	<= not "0000";

					when Main_Init =>
						r_Reset <= '0';
						r_Leds <= "1100";
						--o_Turn_Led 	<= not "1111";
						o_CU_Turn <= r_Turn_Binary;--"11"; --TO CHECK

					when New_Game =>
						r_Reset <= '1';
						r_Leds <= "0100";
						
						--o_Turn_Led 	<= not "0000";

						o_CU_Turn <= r_Turn_Binary;--"11"; --TO CHECK

					when Game_Start =>
						r_Reset <= '0';
						r_Leds <= "0111";
						r_Switch		<= '0';

						o_CU_Turn <= r_Turn_Binary;

						case( r_Turn_Binary ) is
						
							when "11" =>
								o_CU_Buttons(0)	<= '0';-------------------------
								o_CU_Buttons(1)	<= '0';-------------------------

							when "01" =>
								o_CU_Player_Buttons <=  r_P1_Buttons_Stable;
								if (r_Start_DelayCounter /= '1') then
									o_CU_Buttons(0)	<= r_P1_Buttons_Stable(0);
									o_CU_Buttons(1)	<= r_P1_Buttons_Stable(1);		
								end if;
							when "10" =>
								o_CU_Player_Buttons <=  r_P2_Buttons_Stable;

								if (r_Start_DelayCounter /= '1') then

									o_CU_Buttons(0)	<= r_P2_Buttons_Stable(0);
									o_CU_Buttons(1)	<= r_P2_Buttons_Stable(1);
								end if;

							when "00" =>
								o_CU_Player_Buttons(0) <=  r_Buttons_Stable(0);
								o_CU_Buttons(1)	<= '0';------------------------------------------
								o_CU_Buttons(0)	<= '0';
									--o_CU_Buttons(1)	<= '0';	
							when others =>
						
						end case ;

					when Game_Play =>
						r_Reset <= '0';
						--o_CU_Buttons(0)	<= r_Buttons_Stable(0);------------------------------------
						r_Leds <= "1011";
						r_Switch		<= '0';

						o_CU_Turn <= r_Turn_Binary;

						case( r_Turn_Binary ) is
						
							when "11" =>
								o_CU_Buttons(0)	<= '0';-------------------------
								o_CU_Buttons(1)	<= '0';-------------------------
							when "01" =>
								o_CU_Player_Buttons <=  r_P1_Buttons_Stable;
								o_CU_Buttons(0)	<= r_P1_Buttons_Stable(0);
								o_CU_Buttons(1)	<= r_P1_Buttons_Stable(1);-------------------------
							when "10" =>
								o_CU_Player_Buttons <=  r_P2_Buttons_Stable;
								o_CU_Buttons(0)	<= r_P2_Buttons_Stable(0);
								o_CU_Buttons(1)	<= r_P1_Buttons_Stable(1);-------------------------

							when "00" =>
								o_CU_Buttons(0)	<= '0';-------------------------
								o_CU_Buttons(1)	<= '0';-------------------------

							when others =>
						
						end case ;

					when Game_Finish =>
						r_Reset <= '0';
						r_Leds <= "1101";

						o_CU_Buttons(0)	<= '0';
						r_Switch		<= '1';

						case r_Turn_GF is
							when 0 =>
								--o_Turn_Led 	<= not "0000";--0000
								o_CU_Turn <= "11";
							when 1 =>
								--o_Turn_Led 	<= not "0001";--0001
								o_CU_Turn <= "01";
								
							when 2 =>
								--o_Turn_Led 	<= not "0010";--0010
								o_CU_Turn	<= "10";
							when 3 =>
								--o_Turn_Led 	<= not "0100";--0100
								o_CU_Turn <= "00";
							when 4 =>
								--o_Turn_Led 	<= not "1111";--0100
								o_CU_Turn <= "11";
						end case;

					when End_Game =>
						r_Reset <= '0';
						r_Leds <= "0011";
						--o_Turn_Led 	<= not "1111";

					when Idle =>
						r_Reset <= '0';
						r_Leds <= "1000";
						--o_Turn_Led 	<= not "1111";

				end case;	
		end if;
	end process;


o_CU_Reset 		<= r_Reset;
o_CU_FSM_State 	<= Next_State;
o_CU_Buttons(2)	<= r_Buttons_Stable(2);
o_CU_EndGame	<= r_Switch;

end Behavioral;