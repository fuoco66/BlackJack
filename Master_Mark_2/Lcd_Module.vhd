library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Lcd_Constants.ALL;
use work.Constants.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Lcd_Module is
port (
	i_Lcd_Clk		: in std_logic;
	i_Lcd_On 		: in std_logic;
	i_Lcd_Command_Code : in Lcd_Commands_Code_Block(0 to 6);
	o_Lcd_BusyFlag 		: out std_logic;
	o_Lcd_Data		: out std_logic_vector(7 downto 0);		--output bus, used for data transfer (DB)
	o_Lcd_Control	: out std_logic_vector (6 downto 4)


	);
end Lcd_Module;

architecture Behavioral of Lcd_Module is

	TYPE FSM_State_Lcd IS ( Reset,
							Idle,
							SetCommand,
							WriteCommand,
							WriteCommand_Delay);
	
	signal Current_State:		FSM_State_Lcd:= Reset;			
	signal Next_State:			FSM_State_Lcd;

	signal r_Command_Code_OLD : std_logic_vector (6 downto 0) := (others => '0');


	signal r_DelayOK 		: std_logic;
	signal r_Delay_Counter	: integer range 0 to 2097152:= 0;
	signal r_Delay_Power 	: integer range 3 to 20:= 3;

	signal r_Lcd_Cmd_Pointer : integer;

	signal oneUSClk:	std_logic:='0';
	signal clkCount:	integer range 0 to 128;

	signal r_WriteDone : std_logic;
	signal r_initDone : std_logic := '0';

	signal r_Lcd_CurrentCommands 		: Lcd_Commands_Mem(0 to 300) := (others => (others => '0') );
	signal r_Lcd_CurrentCommands_Limit 	: integer range 0 to 300:= 0;

begin
	
	process (i_Lcd_Clk, i_Lcd_On)
	begin
		if (i_Lcd_Clk = '1' and i_Lcd_Clk'event) then
			--if(i_Lcd_On = '1') then
				if(clkCount = 125) then
					clkCount <= 0;
					oneUSClk <= not oneUSClk;
				else 
					clkCount <= clkCount + 1;
				end if;
			--else
				--oneUSClk <= '1';
			--end if;
		end if;
	end process;

	--oneUSClk <= i_Lcd_Clk;

	Delay_proc :process (oneUSClk, r_DelayOK)
	begin
		if (oneUSClk = '1' and oneUSClk'event) then
			if r_DelayOK = '1' or Current_State = Idle or Current_State = Reset then
				r_Delay_Counter <= 0;
			else
				r_Delay_Counter <= r_Delay_Counter + 1;
			end if;
		end if;
	end process;
	
	r_DelayOK <= '1' when (Current_State = WriteCommand_Delay and r_Delay_Counter = 2**r_Delay_Power)	else '0';						

	------- ATTENZIONE ------ è CALIBRATO SUL DELAY COUNTER!!!!!
	Char_Pointer :process (oneUSClk)
   		begin
			if (oneUSClk = '1' and oneUSClk'event) then
				--if (Current_State = WriteCommand_Delay and r_Delay_Counter = 5 ) then
				if (Current_State = WriteCommand_Delay and r_Delay_Counter = 6 ) then
					if(r_initDone = '0') then

						if(r_Lcd_Cmd_Pointer = c_Lcd_Commands_Init'HIGH) then
							r_Lcd_Cmd_Pointer <= 0;
							r_WriteDone <= '1';
							r_initDone <= '1';
						else
							r_Lcd_Cmd_Pointer <= r_Lcd_Cmd_Pointer + 1;
						end if;

					elsif r_initDone = '1' then
						
						if(r_Lcd_Cmd_Pointer = r_Lcd_CurrentCommands_Limit ) then --or r_Lcd_CurrentCommands(r_Lcd_Cmd_Pointer+1)(14 downto 10) = "00000" 
							r_Lcd_Cmd_Pointer <= 0;
							r_WriteDone <= '1';
						else
							r_Lcd_Cmd_Pointer <= r_Lcd_Cmd_Pointer + 1;
						end if;
					end if;
				elsif Current_State = Reset or Next_State = Reset then
					r_Lcd_Cmd_Pointer <= 0;
					r_WriteDone <= '0';

					r_initDone <= '0';
				elsif Current_State = Idle then
					r_WriteDone <= '0';
						r_initDone <= '1';
				else
					r_Lcd_Cmd_Pointer <= r_Lcd_Cmd_Pointer;
				end if;
			
			end if;
		end process;

	Current : process (oneUSClk, i_Lcd_On)
	begin
		if oneUSClk = '1' and oneUSClk'Event then

			if i_Lcd_On = '1' then 
				--r_Command_Code_OLD <= i_Lcd_Command_Code;
				Current_State <= Next_State;
			else
				--r_Command_Code_OLD <= (others => '0');
				Current_State <= Reset;
			end if;
		end if;
	end process;

	o_Lcd_BusyFlag <= '0' when (Current_State = Idle) else '1';

	nextP : process (oneUSClk, i_Lcd_On)
	begin
		if (oneUSClk = '1' and oneUSClk'event) then

			case Current_State is
				when Reset =>
					if i_Lcd_On = '1' then 
						Next_State <= SetCommand;
					elsif Next_State = Reset then
						r_Command_Code_OLD <= (others => '0');
						Next_State <= Reset;
					end if;

				when SetCommand =>
					Next_State <= WriteCommand;

				when WriteCommand =>	
					Next_State <= WriteCommand_Delay;
				
				when WriteCommand_Delay=>
					if r_DelayOK = '1' then
					
						if(r_WriteDone = '1') then
							r_Lcd_CurrentCommands <= (others => (others => '0') ); -- (others => "00111" & "00"&X"02");--return home
							r_Lcd_CurrentCommands_Limit <= 7;
							Next_State <= Idle;
						else
							Next_State <= SetCommand;
						end if;
												
					elsif Next_State = WriteCommand_Delay then
						Next_State <= WriteCommand_Delay;
					end if;

				when Idle =>
					if i_Lcd_On = '1' then
						r_Command_Code_OLD <= i_Lcd_Command_Code(0);
						
						if( i_Lcd_Command_Code(0) /= "0000000" and r_Command_Code_OLD /= i_Lcd_Command_Code(0))then

									for I in 0 to c_Lcd_Commands_Max loop --c_MainCommandBlock( to_integer(unsigned(i_Lcd_Command_Code(1) )) )'HIGH loop

										r_Lcd_CurrentCommands(I) <= c_MainCommandBlock( to_integer(unsigned(i_Lcd_Command_Code(1))) )(I);
										
										if ( i_Lcd_Command_Code(2) /= "0000000") then										
											r_Lcd_CurrentCommands(I+26) <= c_MainCommandBlock( to_integer(unsigned(i_Lcd_Command_Code(2))) )(I);
											
											
											if ( i_Lcd_Command_Code(3) /= "0000000") then
											
												r_Lcd_CurrentCommands(I+52) <= c_MainCommandBlock( to_integer(unsigned(i_Lcd_Command_Code(3))) )(I);

												if ( i_Lcd_Command_Code(4) /= "0000000") then	
													r_Lcd_CurrentCommands(I+78) <= c_MainCommandBlock( to_integer(unsigned(i_Lcd_Command_Code(4))) )(I);
													

													if ( i_Lcd_Command_Code(5) /= "0000000") then	
														
														r_Lcd_CurrentCommands(I+104) <= c_MainCommandBlock( to_integer(unsigned(i_Lcd_Command_Code(4))) )(I);

														if ( i_Lcd_Command_Code(6) /= "0000000") then	
														
															r_Lcd_CurrentCommands(I+130) <= c_MainCommandBlock( to_integer(unsigned(i_Lcd_Command_Code(4))) )(I);
															r_Lcd_CurrentCommands_Limit <= 155;--r_Lcd_CurrentCommands_Limit +1;

														else
															r_Lcd_CurrentCommands_Limit <= 129;--r_Lcd_CurrentCommands_Limit +1;
														end if;



													else
														r_Lcd_CurrentCommands_Limit <= 103;--r_Lcd_CurrentCommands_Limit +1;
													end if;


												else
													r_Lcd_CurrentCommands_Limit <= 77;--r_Lcd_CurrentCommands_Limit +1;
												end if;

											else
												r_Lcd_CurrentCommands_Limit <= 51;--r_Lcd_CurrentCommands_Limit +1;
											end if;

										
										else
											r_Lcd_CurrentCommands_Limit <= 25;--r_Lcd_CurrentCommands_Limit +1;
										end if;

									end loop;

							Next_State <= SetCommand;	

						elsif Next_State = Idle then
							Next_State <= Idle;
							r_Lcd_CurrentCommands_Limit <= 0;
							r_Command_Code_OLD <= (others => '0');

						end if;

					else
						Next_State <= Reset;

					end if;

			end case;

		end if;
	end process;

	output : process (Current_State)
		begin  

			case Current_State is
				when Reset =>
					o_Lcd_Control(4) <= '0';
					o_Lcd_Control(5) <= '0';
					o_Lcd_Data 		 <= (others => '0');

				when SetCommand =>
					if(r_initDone = '0') then
						--r_Delay_Power <= to_integer(unsigned( c_Lcd_Commands_Init(r_Lcd_Cmd_Pointer)(14 downto 10)));
						
						--if (c_Lcd_Commands_Init(r_Lcd_Cmd_Pointer)(14 downto 10) /= "00101" ) then
							r_Delay_Power <= to_integer(unsigned( c_Lcd_Commands_Init(r_Lcd_Cmd_Pointer)(14 downto 10)));
							o_Lcd_Control(4) <= c_Lcd_Commands_Init(r_Lcd_Cmd_Pointer)(9);
							o_Lcd_Control(5) <= c_Lcd_Commands_Init(r_Lcd_Cmd_Pointer)(8);
							o_Lcd_Data 		 <= c_Lcd_Commands_Init(r_Lcd_Cmd_Pointer)(7 downto 0);
						--else
							--r_Delay_Power <= to_integer(unsigned( c_Lcd_Commands_Init(r_Lcd_Cmd_Pointer)(14 downto 10)));
							o_Lcd_Control(4) <= '0';
							--o_Lcd_Control(5) <= '0';
							--o_Lcd_Data 		 <= (others => '0');
						--end if;


						
					elsif(r_initDone = '1')then
						r_Delay_Power <= to_integer(unsigned( r_Lcd_CurrentCommands(r_Lcd_Cmd_Pointer)(14 downto 10)));
						o_Lcd_Control(4) <= r_Lcd_CurrentCommands(r_Lcd_Cmd_Pointer)(9);
						o_Lcd_Control(5) <= r_Lcd_CurrentCommands(r_Lcd_Cmd_Pointer)(8);
						o_Lcd_Data 		 <= r_Lcd_CurrentCommands(r_Lcd_Cmd_Pointer)(7 downto 0);
					end if;
					
								
				when WriteCommand =>
					if(r_initDone = '0') then
						o_Lcd_Control(4) <= c_Lcd_Commands_Init(r_Lcd_Cmd_Pointer)(9);
						o_Lcd_Control(5) <= c_Lcd_Commands_Init(r_Lcd_Cmd_Pointer)(8);
						o_Lcd_Data 		 <= c_Lcd_Commands_Init(r_Lcd_Cmd_Pointer)(7 downto 0);
					elsif(r_initDone = '1')then
						o_Lcd_Control(4) <= r_Lcd_CurrentCommands(r_Lcd_Cmd_Pointer)(9);
						o_Lcd_Control(5) <= r_Lcd_CurrentCommands(r_Lcd_Cmd_Pointer)(8);
						o_Lcd_Data 		 <= r_Lcd_CurrentCommands(r_Lcd_Cmd_Pointer)(7 downto 0);
					end if;
				
				when WriteCommand_Delay =>
					if(r_initDone = '0') then
						o_Lcd_Control(4) <= c_Lcd_Commands_Init(r_Lcd_Cmd_Pointer)(9);
						o_Lcd_Control(5) <= c_Lcd_Commands_Init(r_Lcd_Cmd_Pointer)(8);
						o_Lcd_Data 		 <= c_Lcd_Commands_Init(r_Lcd_Cmd_Pointer)(7 downto 0);
					elsif(r_initDone = '1')then
						o_Lcd_Control(4) <= r_Lcd_CurrentCommands(r_Lcd_Cmd_Pointer)(9);
						o_Lcd_Control(5) <= r_Lcd_CurrentCommands(r_Lcd_Cmd_Pointer)(8);
						o_Lcd_Data 		 <= r_Lcd_CurrentCommands(r_Lcd_Cmd_Pointer)(7 downto 0);
					end if;

				when Idle =>
					o_Lcd_Control(4) <= '0';
					o_Lcd_Control(5) <= '0';
					o_Lcd_Data 		 <= (others => '0');

			end case;


		end process;

		o_Lcd_Control(6) <= '1' when ( (Current_State = WriteCommand) 
										and ( r_Delay_Power >5 ) )
										else '0';
		

		--o_Lcd_Control(6) <= '1' when (Current_State = WriteCommand)	else '0';




end Behavioral;


--if i_Lcd_On = '1' then
--	r_Command_Code_OLD <= i_Lcd_Command_Code;
	
--	if( i_Lcd_Command_Code /= "0000" and r_Command_Code_OLD /= i_Lcd_Command_Code)then
		
--		for I in 0 to c_MainCommandBlock( to_integer(unsigned(i_Lcd_Command_Code)) )'HIGH loop
--			r_Lcd_CurrentCommands(I) <= c_MainCommandBlock( to_integer(unsigned(i_Lcd_Command_Code)) )(I);
--		end loop;
--		r_Lcd_CurrentCommands_Limit <= c_MainCommandBlock( to_integer(unsigned(i_Lcd_Command_Code)) )'HIGH;

--		Next_State <= SetCommand;	

		

--	elsif Next_State = Idle then
--		Next_State <= Idle;
--		r_Command_Code_OLD <= (others => '0');

--	end if;

--else
--	Next_State <= Reset;

--end if;


--case( i_Lcd_Command_Code ) is
							
--	when "0001" => --main_init
--		for I in 0 to c_MainCommandBlock(0)'HIGH loop
--			r_Lcd_CurrentCommands(I) <= c_MainCommandBlock(0)(I);
--		end loop;
--		r_Lcd_CurrentCommands_Limit <= c_MainCommandBlock(0)'HIGH;

--		Next_State <= SetCommand;	

--		--for I in 0 to c_Lcd_Commands_1'HIGH loop
--		--	r_Lcd_CurrentCommands(I) <= c_Lcd_Commands_1(I);
--		--end loop;
--		--r_Lcd_CurrentCommands_Limit <= c_Lcd_Commands_1'HIGH;

--		--Next_State <= SetCommand;	

--	when "0010" => --main_init
--		for I in 0 to c_MainCommandBlock(1)'HIGH loop
--			r_Lcd_CurrentCommands(I) <= c_MainCommandBlock(1)(I);
--		end loop;
--		r_Lcd_CurrentCommands_Limit <= c_MainCommandBlock(1)'HIGH;

--		Next_State <= SetCommand;	

--	when "0011" => --main_init
--		for I in 0 to c_MainCommandBlock(2)'HIGH loop
--			r_Lcd_CurrentCommands(I) <= c_MainCommandBlock(2)(I);
--		end loop;
--		r_Lcd_CurrentCommands_Limit <= c_MainCommandBlock(2)'HIGH;

--		Next_State <= SetCommand;	

--	when "0100" => --main_init
--		for I in 0 to c_MainCommandBlock(3)'HIGH loop
--			r_Lcd_CurrentCommands(I) <= c_MainCommandBlock(3)(I);
--		end loop;
--		r_Lcd_CurrentCommands_Limit <= c_MainCommandBlock(3)'HIGH;

--		Next_State <= SetCommand;	

--	when "0101" => --main_init
--		for I in 0 to c_MainCommandBlock(4)'HIGH loop
--			r_Lcd_CurrentCommands(I) <= c_MainCommandBlock(4)(I);
--		end loop;
--		r_Lcd_CurrentCommands_Limit <= c_MainCommandBlock(4)'HIGH;

--		Next_State <= SetCommand;	


--	when "0110" => --idle
--		for I in 0 to c_MainCommandBlock(5)'HIGH loop
--			r_Lcd_CurrentCommands(I) <= c_MainCommandBlock(5)(I);
--		end loop;
--		r_Lcd_CurrentCommands_Limit <= c_MainCommandBlock(5)'HIGH;

--		Next_State <= SetCommand;	
--		--for I in 0 to c_Lcd_Commands_2'HIGH loop
--		--	r_Lcd_CurrentCommands(I) <= c_Lcd_Commands_2(I);
--		--end loop;
--		--r_Lcd_CurrentCommands_Limit <= c_Lcd_Commands_2'HIGH;

--		--Next_State <= SetCommand;	

--	when others =>
--		Next_State <= Idle;
--		r_Command_Code_OLD <= (others => '0');						
--end case ;