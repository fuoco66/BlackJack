library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Input_Manager is
port (
	i_IM_Clk_125 	: in std_logic;
	i_IM_Buttons	: in std_logic_vector(2 downto 0);
	
	i_IM_P1_Buttons	: in std_logic_vector (1 downto 0);
	i_IM_P2_Buttons	: in std_logic_vector (1 downto 0);

	o_IM_Buttons	: out std_logic_vector(2 downto 0);
	o_IM_P1_Buttons	: out std_logic_vector (1 downto 0);
	o_IM_P2_Buttons	: out std_logic_vector (1 downto 0)
	);
end Input_Manager;

architecture Behavioral of Input_Manager is

	Component Debounce_Switch is
    Port ( i_DebounceS_Clk : in  STD_LOGIC;
           i_DebounceS_Btn : in  STD_LOGIC;
           o_DebounceS_Btn : out  STD_LOGIC);
	end Component Debounce_Switch;

	signal r_Btn_Debounced_1, r_Btn_Debounced_2, r_Btn_Debounced_3 : std_logic;

begin
	
	Deb_Mod_1	: Debounce_Switch port map(i_IM_Clk_125, i_IM_Buttons(0), o_IM_Buttons(0));
	Deb_Mod_2 	: Debounce_Switch port map(i_IM_Clk_125, i_IM_Buttons(1), o_IM_Buttons(1));
	Deb_Mod_3 	: Debounce_Switch port map(i_IM_Clk_125, i_IM_Buttons(2), o_IM_Buttons(2));
	
	Deb_Mod_4 	: Debounce_Switch port map(i_IM_Clk_125, i_IM_P1_Buttons(0), o_IM_P1_Buttons(0));
	Deb_Mod_5 	: Debounce_Switch port map(i_IM_Clk_125, i_IM_P1_Buttons(1), o_IM_P1_Buttons(1));

	Deb_Mod_6 	: Debounce_Switch port map(i_IM_Clk_125, i_IM_P2_Buttons(0), o_IM_P2_Buttons(0));
	Deb_Mod_7 	: Debounce_Switch port map(i_IM_Clk_125, i_IM_P2_Buttons(1), o_IM_P2_Buttons(1));

end Behavioral;



--signal r_Btn_Debounced, r_Btn_Old: std_logic_vector(2 downto 0);

--begin
	
--	Deb_Mod_1	: Debounce_Switch port map(i_IM_Clk_125, i_IM_Buttons(0), r_Btn_Debounced(0));
--	Deb_Mod_2 	: Debounce_Switch port map(i_IM_Clk_125, i_IM_Buttons(1), r_Btn_Debounced(1));
--	Deb_Mod_3 	: Debounce_Switch port map(i_IM_Clk_125, i_IM_Buttons(2), r_Btn_Debounced(2));

--	Buttons_P : process (i_IM_Clk_125)
--	begin
--		if (i_IM_Clk_125 = '1' and i_IM_Clk_125'event) then
--		r_Btn_Old(0)<= r_Btn_Debounced(0);
--		r_Btn_Old(1)<= r_Btn_Debounced(1);
--		r_Btn_Old(2)<= r_Btn_Debounced(2);

		
--		if(r_Btn_Debounced(0) = '1' and r_Btn_Old(0) /= '1') then
--			o_IM_Buttons(0) <= '1';
--		else
--			o_IM_Buttons(0) <= '0';
-- 	 	end if;

-- 	 	if(r_Btn_Debounced(1) = '1' and r_Btn_Old(1) /= '1') then
--			o_IM_Buttons(1) <= '1';
--		else
--			o_IM_Buttons(1) <= '0';
-- 	 	end if;

-- 	 	if(r_Btn_Debounced(2) = '1' and r_Btn_Old(2) /= '1') then
--			o_IM_Buttons(2) <= '1';
--		else
--			o_IM_Buttons(2) <= '0';
-- 	 	end if;

--		end if;
--	end process;
