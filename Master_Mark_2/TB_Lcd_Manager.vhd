--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   18:20:04 07/02/2016
-- Design Name:   
-- Module Name:   D:/Develop/Xlinx/PROJECT/Master_Mark_2/TB_Lcd_Manager.vhd
-- Project Name:  Master_Mark_2
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Lcd_Manager
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use work.Constants.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB_Lcd_Manager IS
END TB_Lcd_Manager;
 
ARCHITECTURE behavior OF TB_Lcd_Manager IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Lcd_Manager
    PORT(
         i_LcdManager_Clk : IN  std_logic;
         i_LcdManager_FSM_State : IN  FSM_State_Main;
         i_LcdManager_Switch_1 : IN  std_logic;
			i_LcdManager_Selector 	: in std_logic_vector(1 downto 0);
         --i_LcdManager_Switch_2 : IN  std_logic;
			i_LcdManager_Turn 	: in std_logic_vector(1 downto 0);
			
			i_LcdManager_P1_Card1	: in std_logic_vector (5 downto 0);
			i_LcdManager_P1_Card2	: in std_logic_vector (5 downto 0);
			i_LcdManager_P2_Card1	: in std_logic_vector (5 downto 0);
			i_LcdManager_P2_Card2	: in std_logic_vector (5 downto 0);

         o_LcdManager_Data_1 : OUT  std_logic_vector(7 downto 0);
         o_LcdManager_Control_1 : OUT  std_logic_vector(6 downto 4);
         o_LcdManager_Data_2 : OUT  std_logic_vector(7 downto 0);
         o_LcdManager_Control_2 : OUT  std_logic_vector(6 downto 4)
        );
    END COMPONENT;
    

   --Inputs
   signal i_LcdManager_Clk : std_logic := '0';
   signal i_LcdManager_FSM_State : FSM_State_Main := Reset;
   signal i_LcdManager_Switch_1 : std_logic := '0';
	signal i_LcdManager_Selector 	: std_logic_vector(1 downto 0);  
	signal i_LcdManager_Turn 	: std_logic_vector(1 downto 0);
	signal i_LcdManager_P1_Card1	: std_logic_vector (5 downto 0);
	signal i_LcdManager_P1_Card2	: std_logic_vector (5 downto 0);
	signal i_LcdManager_P2_Card1	: std_logic_vector (5 downto 0);
	signal i_LcdManager_P2_Card2	: std_logic_vector (5 downto 0);

 	--Outputs
   signal o_LcdManager_Data_1 : std_logic_vector(7 downto 0);
   signal o_LcdManager_Control_1 : std_logic_vector(6 downto 4);
   signal o_LcdManager_Data_2 : std_logic_vector(7 downto 0);
   signal o_LcdManager_Control_2 : std_logic_vector(6 downto 4);

   -- Clock period definitions
   constant i_LcdManager_Clk_period : time := 8 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Lcd_Manager PORT MAP (
          i_LcdManager_Clk => i_LcdManager_Clk,
          i_LcdManager_FSM_State => i_LcdManager_FSM_State,
          i_LcdManager_Switch_1 => i_LcdManager_Switch_1,
			 i_LcdManager_Selector => i_LcdManager_Selector,
			 i_LcdManager_Turn => i_LcdManager_Turn,
          i_LcdManager_P1_Card1 => i_LcdManager_P1_Card1,
			 i_LcdManager_P1_Card2 => i_LcdManager_P1_Card2,
			 i_LcdManager_P2_Card1 => i_LcdManager_P2_Card1,
			 i_LcdManager_P2_Card2 => i_LcdManager_P2_Card2,
			 o_LcdManager_Data_1 => o_LcdManager_Data_1,
          o_LcdManager_Control_1 => o_LcdManager_Control_1,
          o_LcdManager_Data_2 => o_LcdManager_Data_2,
          o_LcdManager_Control_2 => o_LcdManager_Control_2
        );

   -- Clock process definitions
   i_LcdManager_Clk_process :process
   begin
		i_LcdManager_Clk <= '0';
		wait for i_LcdManager_Clk_period/2;
		i_LcdManager_Clk <= '1';
		wait for i_LcdManager_Clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin	
		i_LcdManager_FSM_State <= Reset;
		wait for 100 ns;
		
	--	i_LcdManager_Switch_1 <= '0';
		i_LcdManager_FSM_State <= Reset_Delay;
		wait for 30ms;
		
		i_LcdManager_FSM_State <= Idle;
		wait for 100 ms;
--		i_LcdManager_Switch_1 <= '1';

		i_LcdManager_FSM_State <= New_Game;
      wait for 100ms;
		i_LcdManager_FSM_State <= Game_Start;

      wait;
   end process;

END;
