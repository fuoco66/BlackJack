library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.Constants.ALL;



entity Comm_Ram_Manager is
port(
	i_Master_Clk : in std_logic;
	i_Master_On : in std_logic;

	i_Master_Status 		: FSM_state_Main;
	i_Master_Turn			: std_logic_vector (1 downto 0);
	
	i_Master_Dealer_Sum	: integer range 0 to 63;	
	i_Master_Player1_Sum	: integer range 0 to 63;
	i_Master_Player2_Sum	: integer range 0 to 63;


	o_Master_VRam_Select	: out std_logic_vector (0 downto 0);
	o_Master_VRam_Address	: out std_logic_vector (15 downto 0);
	o_Master_VRam_Data 	: out std_logic_vector (2 downto 0)
	);
end Comm_Ram_Manager;

architecture Behavioral of Comm_Ram_Manager is

	Component PrinterManager is
	port(
	i_PrintModule_Clk : in std_logic;
	i_PrintModule_Reset	: in std_logic;

	i_PrintModule_State : in FSM_state_Main;
	i_PrintModule_Turn	: in std_logic_vector (1 downto 0);
	
	i_Ps_Dealer_Sum		: in integer range 0 to 63;
	i_Ps_Player1_Sum	: in integer range 0 to 63;
	i_Ps_Player2_Sum	: in integer range 0 to 63;

	o_Printmodule_Request : out std_logic;
	o_Printmodule_Code_Object	: out integer range 0 to 63;
	o_Printmodule_X_Object		: out integer range 0 to 511;
	o_Printmodule_Y_Object		: out integer range 0 to 127
	);
	end Component;

	Component Rom_Printer is
	port (
		i_Printer_Clk			: in std_logic;
		i_Printer_Reset 		: in std_logic;

		i_Printer_Request		: in std_logic;
		i_Printer_Code_Object	: in integer range 0 to 63;
		i_Printer_X_Object		: in integer range 0 to 511;
		i_Printer_Y_Object		: in integer range 0 to 127;

		o_Printer_VRam_Select	: out std_logic_vector (0 downto 0);
		o_Printer_VRam_Address	: out std_logic_vector (15 downto 0);
		o_Printer_VRam_Data 	: out std_logic_vector (2 downto 0)
		);
	end Component;

	signal r_Tmp_Request : std_logic;
	signal r_Tmp_Code_Object : integer range 0 to 63;
	signal r_Tmp_X_Object : integer range 0 to 511;
	signal r_Tmp_Y_Object : integer range 0 to 127;

begin

	Rom_Print	: Rom_Printer port map (
		i_Master_Clk,
		i_Master_On,
		r_Tmp_Request,
		r_Tmp_Code_Object,
		r_Tmp_X_Object,
		r_Tmp_Y_Object,
		o_Master_Vram_Select,
		o_Master_VRam_Address,
		o_Master_VRam_Data
		);

	Printer_Mod : PrinterManager port map (
		i_Master_Clk,
		i_Master_On,
		i_Master_Status,
		i_Master_Turn,
		i_Master_Dealer_Sum,
		i_Master_Player1_Sum,
		i_Master_Player2_Sum,
		r_Tmp_Request,
		r_Tmp_Code_Object,
		r_Tmp_X_Object,
		r_Tmp_Y_Object
		);

end Behavioral;

