library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Music_Constants.ALL;
use work.Constants.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Music_Module is
port(
	i_Music_Clk		: in std_logic;
	--i_Music_On  : in std_logic;
	--i_Music_Switch  : in std_logic;
	i_Music_FSM_State 	: in FSM_State_Main;
	o_Music_Out 		: out std_logic
	);
end Music_Module;

architecture Behavioral of Music_Module is
	
	
	signal r_Counter : integer;
	signal r_Counter_Limit : integer;
	signal r_seconds : integer range 0 to 250000000:=0;

	signal r_Nota, r_Nota_OLD : integer :=0;-- range 0 to c_Music_Pirates'HIGH:=0;
	signal r_Pin, r_Music_Switch_Old :std_logic;
	signal r_MainFSM_OLD : FSM_State_Main;

begin
	
	process(i_Music_Clk)
		begin
			if i_Music_Clk'event and i_Music_Clk = '1' then
				if ( i_Music_FSM_State /= Reset) then
					
		--begin(r_MainFSM_OLD /= i_LcdManager_FSM_State) and ( i_LcdManager_FSM_State = Idle )
					--(i_Music_Switch /= r_Music_Switch_Old)
					if (r_MainFSM_OLD /= i_Music_FSM_State) and (i_Music_FSM_State = End_Game)  then
						r_Nota <= 0;
						r_seconds <= 0;
					else
						if (i_Music_FSM_State /= End_Game) and (i_Music_FSM_State /= Main_Init) then
							
							if (r_seconds >= to_integer(unsigned( c_Music_Pirates(r_Nota)(35 downto 8) )) and r_Nota < c_Music_Pirates'HIGH) then
								r_seconds <= 0;
								r_Nota <= r_Nota +1;
							elsif r_Nota = c_Music_Pirates'HIGH then
								r_Nota <= 0;
							else
								r_seconds <= r_seconds +1;
								r_Nota <= r_Nota;					
							end if;

						elsif (i_Music_FSM_State = End_Game) then
							
							if (r_seconds >= to_integer(unsigned( c_Music_Mario_Loose(r_Nota)(35 downto 8) )) and r_Nota < c_Music_Mario_Loose'HIGH) then
								r_seconds <= 0;
								r_Nota <= r_Nota +1;
							elsif r_Nota = c_Music_Mario_Loose'HIGH then
								r_Nota <= 0;
							else
								r_seconds <= r_seconds +1;
								r_Nota <= r_Nota;					
							end if;

						end if;

					end if;
				
				else
					r_seconds <= 0;
					r_seconds <= 0;
					r_Nota <= 0;
				end if;
			end if;

	end process;

	process (i_Music_Clk)
	begin
		if i_Music_Clk'event and i_Music_Clk = '1' then
			if ( i_Music_FSM_State /= Reset) then
				r_Nota_OLD <= r_Nota;
				--r_Music_Switch_Old <= i_Music_Switch;

				r_MainFSM_OLD <= i_Music_FSM_State;

				if (i_Music_FSM_State /= End_Game) then
					r_Counter_Limit <= c_Music_Notes( to_integer(unsigned(c_Music_Pirates(r_Nota)(7 downto 0))) ) ;
				elsif (i_Music_FSM_State = End_Game) and (i_Music_FSM_State /= Main_Init) then
					r_Counter_Limit <= c_Music_Notes( to_integer(unsigned(c_Music_Mario_Loose(r_Nota)(7 downto 0))) ) ;
				end if;

			else
				r_Counter_Limit <= 0;
			end if;
				
		end if;
	end process;


	process (i_Music_Clk)
	begin
		if i_Music_Clk'event and i_Music_Clk = '1' then
			if ( i_Music_FSM_State /= Reset) then
				
				
				if( r_Nota /= r_Nota_OLD and r_Counter_Limit /= 0)then
					r_Counter <= 0;
					r_Pin <= '0';
				elsif ( r_Nota /= r_Nota_OLD and r_Counter_Limit = 0)then
					r_Counter <= 0;
					r_Pin <= '0';
				elsif r_Counter < r_Counter_Limit/2 and r_Counter_Limit /= 0 then
					r_Counter <= r_Counter + 1;				
				elsif r_Counter = r_Counter_Limit/2 and r_Counter_Limit /= 0 then
					r_Pin <= not r_Pin;
					r_Counter <= 0;
				end if;



			else
				r_Counter <= 0;
				r_Pin <= '0';
			end if;
		end if;
	end process;


	o_Music_Out <= r_Pin;


end Behavioral;