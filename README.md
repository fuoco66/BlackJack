BlackJack - Project

This is a version of the Black Jack card game, entirely realized in VHDL as a final project for the course of Computer Architecture (Politecnico of Turin, a.y. 2015-2016).

You can find the source code in the folder "Master_Mark_2"
The project has been created using Xilinx ISE Design Suite 14.
The master file of the project is "Master_Mark_2.xise".

If you want to try the game upload the file "BlackJack.bit" on your board.

Copyright (c) 2017  -  Emanuele Fuoco & Stefano Zamboni
Emanuele: "emanuelefuoco@gmail.com"
Stefano: "stefano.zamboni95@gmail.com"


NOTE:
The project is only complatible for a Xilinx ZYBO Zinq-7000 board, and requires a special hardware interface to be operated.
For more informations and technical specification feel free to contact us.